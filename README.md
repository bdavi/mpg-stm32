# MPG STM32 libraries

This repository aims to collect a clean, well-documented set of software that provides methods for a standardized interface to STM32-based systems. Its main functionality is to allow for monitoring, logging, in-runtime modification of parameters, and provides wrapper classes for commonly used peripherals such as I2C.

It does *not* act as replacement for the STM32CubeIDE. In fact, any of the major hardware setup will be left to the STM Code Generation and User, unless specific settings are required. This library is thusly best used in combination with the STM32CubeIDE.

### Usage
Using this library requires the following:
- Addition of the root of this folder to the include path. 
  - "Project"->"Properties"->"C/C++ Build"->"Settings"->"Include Paths"
  - The path must be added for the C and C++ compiler!
- Addition of the relevant source file directories to the compiler source path.
  - Create a new root src folder. Right-Click->New->Source Folder. Recommended name: mpg-stm
  - Addition of relevant source folders via links. Right-Click on the mpg-stm folder->New->Folder->Advanced->Link To Alternate Location
  - Link to the "all" folder, as well as any device specific folders you require.