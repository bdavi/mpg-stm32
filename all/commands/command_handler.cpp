/**
 * @author David Bailey (davidbailey.2889@gmail.com) 
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#include <all/commands/command_handler.h>
#include <all/log/log.h>

#include <cstdlib>
#include <string.h>

char CommandBuffer::reply_buffer[128] = {};
int CommandBuffer::reply_buffer_pos = 0;

CommandBuffer::CommandBuffer() :
	cmd_buffer_ptr(0), cmd_buffer(),
	wait_for_cmd_end(false),
	cmd_puts_fn(nullptr) {
}

void CommandBuffer::puts(const char * c, char delim) {
	if(c == nullptr)
		return;
	if(reply_buffer_pos >= (sizeof(reply_buffer)-1))
		return;

	size_t len = 0;

	char * e_ptr = strchr(c, delim);
	if(e_ptr == nullptr)
		len = strlen(c);
	else
		len = e_ptr - c;

	if(len > (sizeof(reply_buffer)-reply_buffer_pos-1))
		len = sizeof(reply_buffer)-reply_buffer_pos-1;

	memcpy(reply_buffer+reply_buffer_pos, c, len);
	reply_buffer_pos += len;
}

void CommandBuffer::exec(const char * data) {
	if(data == nullptr)
		return;
	char answer_tag[16] = {};

	while(*data == ' ') {
		data++;
	}

	if(*data == '[') {
		unsigned int i = 0;
		data++;

		while(*data != ']') {
			if(*data == 0)
				return;

			if(i < sizeof(answer_tag)-1)
				answer_tag[i++] = *data;

			data++;
		}

		data++;
	}


	while(*data == ' ') {
		data++;
	}

	if(*data == 0)
		return;

	cmd_start_ptr = data;

	if(cmd_exec_fn) {
		reply_buffer_pos = 0;

		if(answer_tag[0] != 0) {
			this->puts("CMD/");
			this->puts(answer_tag);
			this->puts(": ");
		}
		else {
			this->puts("CMD: ");
		}
		cmd_exec_fn(*this);

		this->puts("\n");

		reply_buffer[reply_buffer_pos] = 0;

		if(cmd_puts_fn)
			cmd_puts_fn(reply_buffer);
	}
}

void CommandBuffer::feed(const char * data) {
	if(data == nullptr)
		return;

	while(1) {
		if(*data == 0)
			return;

		if(wait_for_cmd_end) {
			if(*data == '\n') {
				wait_for_cmd_end = false;
				cmd_buffer_ptr = 0;
			}

			data++;
			continue;
		}

		if((*data == '\n') || (*data == '\r')) {
			cmd_buffer[cmd_buffer_ptr] = 0;
			cmd_buffer_ptr = 0;
			exec(cmd_buffer);
		}
		else if((*data == 0x08) || (*data == 127)) {
			if(cmd_buffer_ptr > 0)
				cmd_buffer_ptr--;
		}
		else if(*data == 27) {
			cmd_buffer_ptr = 0;
		}
		else if((*data >= ' ') && (*data <= '~')) {
			cmd_buffer[cmd_buffer_ptr++] = *data;
			if(cmd_buffer_ptr >= 2048) {
				LOGE("CMD", "Command length exceeds buffer length, skipping");
				wait_for_cmd_end = true;
			}
		}
		data++;
	}
}

bool CommandBuffer::cmd_equal(const char * name) {
	if(cmd_start_ptr == nullptr)
		return false;
	if(name == nullptr)
		return false;

	auto lname = cmd_start_ptr;

	while(1) {
		if(*name == 0 && (*lname == ' ' || *lname == 0))
			return true;
		if(*name != *lname)
			return false;

		name++;
		lname++;
	}
}

const char * CommandBuffer::get_arg(int i) {
	if(cmd_start_ptr == nullptr)
		return nullptr;

	auto lptr = cmd_start_ptr;

	while(1) {
		while(1) {
			if(*lptr == 0)
				return nullptr;
			if(*lptr == ' ')
				break;

			lptr++;
		}

		while(*lptr == ' ')
			lptr++;

		if(*lptr == 0)
			return nullptr;

		if(i == 0)
			return lptr;

		i--;
	}
}

uint8_t char_to_bits(char c, bool & ok) {
	if(c >= '0' && c <= '9')
		return c - '0';
	if(c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	if(c >= 'A' && c <= 'F')
		return c - 'A' + 10;

	ok = false;
	return 0;
}

bool CommandBuffer::parse_long(int i, long int & data) {
	auto arg_ptr = get_arg(i);
	if(arg_ptr == nullptr)
		return false;

	char * end_ptr = nullptr;
	data = strtol(arg_ptr, &end_ptr, 0);

	return true;
}

bool CommandBuffer::parse_hex(int i, void * buffer_v, size_t & size) {
	const char * arg_ptr = get_arg(i);
	if(arg_ptr == nullptr)
		return false;

	uint8_t * buffer = reinterpret_cast<uint8_t*>(buffer_v);

	size_t processed_len = 0;
	while(1) {
		if(*arg_ptr == ' ' || *arg_ptr == 0) {
			size = processed_len;
			return true;
		}

		if(processed_len >= size)
			return false;

		bool ok = true;
		uint8_t next_byte = (char_to_bits(*(arg_ptr), ok) << 4) | (char_to_bits(*(arg_ptr+1), ok));
		arg_ptr += 2;
		if(!ok)
			return false;

		*buffer = next_byte;
		buffer++;
		processed_len++;
	}
}
