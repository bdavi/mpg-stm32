/**
 * @file command_handler.h
 * @author David Bailey (davidbailey.2889@gmail.com)
 * @brief Ingests ASCI character streams, parses and executes commands within
 * @version 1.0
 * @date 2022-11-29
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#ifndef SRC_COMMANDHANDLER_H_
#define SRC_COMMANDHANDLER_H_

#include <stddef.h>

class CommandBuffer {
private:
	static char reply_buffer[128];
	static int  reply_buffer_pos;

	int  cmd_buffer_ptr;
	char cmd_buffer[512];

	const char * cmd_start_ptr;

	bool wait_for_cmd_end;

public:
	void (*cmd_puts_fn)(const char *);
	void (*cmd_exec_fn)(CommandBuffer &);

	CommandBuffer();

	void puts(const char * c, char delim = 0);

	void exec(const char *cmd);
	void feed(const char * data);

	bool cmd_equal(const char *name);

	const char * get_arg(int i);

	bool parse_long(int i, long int & data);
	bool parse_hex(int i, void * buffer, size_t & size);
};

#endif /* SRC_COMMANDHANDLER_H_ */
