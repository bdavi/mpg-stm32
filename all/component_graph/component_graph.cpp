/*
 * component_graph.cpp
 *
 *  Created on: 5 May 2022
 *      Author: xaseiresh
 */


#include "component_graph.h"
#include <FreeRTOS.h>
#include <semphr.h>

#include <cstring>
#include <algorithm>

#include <all/log/log.h>

namespace CGRAPH {

const char * const child_failed_msg = "CHECK CHILD";

const char * const state_names[] = {
		"HIDDEN",
		"NOMINAL",
		"SUSPICIOUS",
		"UNINITIALIZED",
		"FAILING",
		"FAILED"
};
const char * const state_colors[] = {
		"",
		STR_COLOR_GREEN,
		STR_COLOR_YELLOW,
		STR_COLOR_PURPLE,
		STR_COLOR_RED,
		STR_COLOR_REDB
};
const log_level_t log_levels[] = {
	LOG_DEBUG,
	LOG_INFO,
	LOG_WARNING,
	LOG_WARNING,
	LOG_ERROR,
	LOG_FATAL
};

// Internal buffer to prevent graph modification
// during certain sequences.
StaticSemaphore_t graph_mutex_buffer;
xSemaphoreHandle  graph_mutex = 0;
TaskHandle_t graph_held_task = 0;

void init() {
	graph_mutex = xSemaphoreCreateMutexStatic(&graph_mutex_buffer);
}

// Quasi-Recursive locking mutex. Default FreeRTOS Mutexes do not initially support recursion and
// require an extra buffer. As we do not need true recursion, we just need to prevent a nested 
// function from double-locking (or unlocking) the mutex, we can check if we are the task that
// is holding the lock already, and then simply not lock or unlock within this function.
#define lock_graph() bool graph_lock_already_held = (xTaskGetCurrentTaskHandle() == graph_held_task);\
 	do {\
		 if(!graph_lock_already_held) {\
		 	xSemaphoreTake(graph_mutex, portMAX_DELAY);\
			graph_held_task = xTaskGetCurrentTaskHandle();\
		 }\
 	} while(0)

#define unlock_graph() do {\
		if(!graph_lock_already_held) {\
			graph_held_task = 0;\
			xSemaphoreGive(graph_mutex);\
		}\
	} while(0)

Component::Component(const char * tag)
		: tag(tag) {

	parent = nullptr;
	child  = nullptr;
	next   = nullptr;

	state_msg = nullptr;
	component_state = UNINITIALIZED;

	parent_relevancy  = SUSPICIOUS;
}

void Component::attach_to(Component & parent) {
	assert(!this->parent);

	lock_graph();

	this->parent = &parent;

	if(parent.child == nullptr) {
		parent.child = this;
	}
	else {
		this->next = parent.child;
		parent.child = this;
	}
	
	// To be safe, make parent recheck state
	parent.update_child_state();
	
	unlock_graph();
}
void Component::set_parent_relevancy(component_state_t relevancy) {
	if(relevancy == this->parent_relevancy)
		return;

	this->parent_relevancy = relevancy;

	if(this->parent)
		// To be safe, make parent recheck state
		this->parent->update_child_state();
}

Component * Component::get_child_component() {
	return child;
}
Component * Component::get_child_component(const char * name) {
	if(name == nullptr)
		return nullptr;

	// This function will descend down the component tree if 
	// it sees a dot separator. strcspn will look for the dot 
	// or it will return the length of the entire string if 
	// no dot was found.
	auto string_compare_length = strcspn(name, ".");

	auto ptr = child;
	while(ptr) {
		// Only compare up to the dot position
		if(strncmp(name, ptr->tag, string_compare_length) == 0) {
			name += string_compare_length;

			if(*name == 0) // We reached the compare input end and are done
				return ptr;
			if(*name != '.') // There was a different separator or other glitch, return nullptr
				return nullptr;

			name++;
			string_compare_length = strcspn(name, ".");

			// Descend into the child
			ptr = ptr->child;
			continue;
		}

		ptr = ptr->next;
	}

	// No child with matching name was found, return nullptr
	return nullptr;
}
Component * Component::get_next() {
	return next;
}
Component * Component::get_parent() {
	return parent;
}

#define SET_MAX_OF(first, second) first = (second > first) ? second : first
void Component::update_child_state() {
	lock_graph();

	auto next_child_state = HIDDEN;

	// This function will get the Maximum of all children's state, limited by the child's relevancy
	for(auto ptr = child; ptr != nullptr; ptr = ptr->next) {
		SET_MAX_OF(next_child_state, std::min(ptr->parent_relevancy, ptr->get_state()));
	}

	if(next_child_state != child_state) {
		child_state = next_child_state;
		update_state();
	}

	unlock_graph();
}

void Component::update_state() {
	lock_graph();

	bool changes = false;

	auto next_state = component_state;
	state_msg = component_state_msg;

	if(child_state > component_state) {
		next_state = child_state;
		// If the child state is determining component state, and 
		// a child is worse than nominal, a different default message is used
		// to tell the user that a child component is having problems
		if(next_state > NOMINAL)
			state_msg = child_failed_msg;
	}

	if(next_state != state)
		changes = true;
	state = next_state;

	if(changes && parent)
		parent->update_child_state();

	unlock_graph();
}

void Component::set_state(component_state_t state, const char * msg) {
	lock_graph();

	component_state_msg = msg;

	if(state == component_state)
		return;

	component_state = state;

	log_message(log_levels[component_state], "CGRAPH", "Component %s switched to %s", tag, state_names[state]);

	update_state();

	unlock_graph();
}

component_state_t Component::get_state() {
	return state;
}
const char * Component::get_state_name() {
	return state_names[state];
}
const char * Component::get_state_message() {
	if(state_msg)
		return state_msg;

	return state_names[state];
}

const char * Component::get_colorization() {
	return state_colors[state];
}

bool Component::set_parameter(const char * key, const char *data) {
	return false;
}
bool Component::get_parameter(const char * key, char * buffer, size_t max_len) {
	return false;
}
const char * Component::get_parameter_name(int i) {
	return nullptr;
}

}
