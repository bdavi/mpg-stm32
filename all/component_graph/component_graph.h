/**
 * @file all/component_graph/component_graph.h
 * @author David Bailey (david.bailey@ipp.mpg-hgw.de)
 * @brief Provides a status and component graph of different elements of the code
 * @version 0.1
 * @date 2022-05-04
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <stddef.h>
#include <stdint.h>

namespace CGRAPH {

/**
 * @brief Component Status enum
 * @details This enum defines the different states a component can be in.
 *   - HIDDEN:  The component will be excluded from prints and is inconspicuous
 *   - NOMINAL: The component and its children are operating as expected.
 *   - SUSPICIOUS: A potential anomaly has been detected in this or the sub-components.
 *   - UNINITIALIZED: Initial value of each component's state, to indicate code has not 
 *      started it up yet.
 *   - FAILING: A value is close to leaving operating range, or has left op. range, and the
 *      component is showing signs of failure
 *   - FAILED: The component has failed and is no longer operational
 *
 * Developer note: This enum must be kept in sync with the internal
 *   arrays "state_names", "state_colours" and "log_levels", to
 *   ensure that each state is appropriately defined
 * 
 */
enum component_state_t : int8_t {
	HIDDEN,
	NOMINAL,
	SUSPICIOUS,
	UNINITIALIZED,
	FAILING,
	FAILED
};

/**
 * @brief Initialize the component graph internal mutexes
 * 
 */
void init();

/**
 * @brief Component element class
 * @details This class' instances represent components of a larger unit.
 *   Each Component may have children as well as a single parent. They thusly form
 *   a component tree.
 * 
 *   Each component may also define an arbitrary value getter/setter functionality, which is to be
 *   used to implement arbitrary configuration parameters. This unifies and greatly eases the way
 *   in which different system can be read and written to.
 *   See set_parameter() and get_parameter() for more information.
 * 
 *   Each component can be in a given set of states. This state indicates the health of the component,
 *   and is used to notify the operator of problems with the hard- and software. Most components
 *   will be in a "NOMINAL" state as they operate. As errors occur, the system can move a component
 *   into "SUSPICIOUS" state, then "FAILING" or "FAILED", to indicate unreliable or no operation.
 *   Additionally, a string message can be supplied to identify the exact error.
 *   See set_state() for more information.
 * 
 *   The component tree is also used to pass along this state information. If a child state degrades, this
 *   is passed along to the parent state. This allows a quick identification of fault sources even for
 *   larger component trees by only inspecting components with faulty states.
 *   See set_parent_relevancy() for more information on this behavior.
 */
class Component {
protected:
	// Linked-List parameters to form the component tree.
	Component * parent; //!< Pointer to immediate parent. Only the root node should have this be nullptr
	Component * child;  //!< Pointer to start of the children-list. May be nullptr
	Component * next;   //!< Pointer to next sibling in the list of components. Nullptr if at end of list

	//! Fault-Indicator message of just this component
	const char *  component_state_msg;
	//! State of just this component (independent of children, vs. state which takes child state into account)
	component_state_t component_state;

	//! Calculated worst state of all children+relevancy
	component_state_t child_state;

	//! Fault-Message of this Component. Will be component_state_msg if there are no child errors,
	//! else will point to the child's error message.
	const char * state_msg;
	//! Calculated state of this Component. Maximum of component_state and child_state.
	component_state_t state;

	//! Parent relevancy. Determines up to what level this component can disrupt the parent component.
	//! Default value is "SUSPICOUS"
	component_state_t parent_relevancy;

	/**
	 * @brief Internal function to re-check child status
	 * @details This function is mainly called by a child's update_state() function after it
	 *   changes its state. This function then re-evaluates its children to recalculate child_state, 
	 *   and, if a change is detected, will call update_state() (which may then later call update_child_state() 
	 *   on the next parent - propagating the state change up).
	 * 
	 */
	void update_child_state();

	/**
	 * @brief Internal function to re-calculate state
	 * @details This function will check "child_state" and "component_state"
	 *   to calculate the new state of this component.
	 *   If the component state has changed, it will call update_child_state() on the parent
	 *   component.
	 *   
	 *   Called internally by set_state
	 */
	void update_state();

public:
	//! Descriptive name of this component. Should remain constant throughout the lifetime of this Component
	const char * const tag;

	/**
	 * @brief Construct a new Component object
	 * @details Construct a new Component with given tag name. After construction, the Component may be linked
	 *   to a parent by using attach_to().
	 *   This Component can immediately be used to log the state of a unit or subsystem using set_state()
	 * 
	 * @param tag Name of this Component. Must remain constant and available throughout the lifetime of this 
	 *   component
	 */
	Component(const char * tag);

	/**
	 * @brief Set the state of this Component
	 * @details This is the main way for the user to report the state of this Component. Whenever a change
	 *   in component state occured, this function should be called, with the new state and an optional
	 *   description of the state given. 
	 *   If the new state is the same as the old state, no further action is taken, and the function
	 *   exits immediately. Else, a log message will be printed describing the state change, and 
	 *   the change is propagated up the component tree (depending on the parent_relevancy setting).
	 *  
	 *   Please note that it is highly recommended to write a single function that analyses the component
	 *   state and handles setting it. Setting the state in multiple locations might cause this 
	 *   component's state to "flicker", leading to console spam. A single function that checks
	 *   for all relevant failure modes (starting at the most severe and going down) and sets the 
	 *   correct state is recommended.
	 * 
	 *   It is also encouraged to encapsulate different tests into separate Component objects and utilize 
	 *   the state propagation.
	 * 
	 * @param state New state that this component will switch to.
	 * @param msg Optional pointer to a null-terminated string describing the state. Not copied internally,
	 *   must remain valid!
	 */
 	void set_state(component_state_t state, const char * msg = nullptr);

	/**
	 * @brief Attach to a parent component
	 * @details This function will attach this component to a parent
	 *   component. This means that it will be added to the linked list
	 *   of children of "parent", and is thusly available to be fetched by
	 *   "get_child_component". This also means that its state will be 
	 *   propagated upwards according to the parent_relevancy setting
	 * 
	 *   Note that this can only be done once. Currently, re-linking the 
	 *   component to a different parent is not supported.
	 * 
	 * @param parent Parent to attach to.
	 */
	void attach_to(Component & parent);

	/**
	 * @brief Set the parent relevancy status
	 * @details This function will appropriately set the parent relevancy. 
	 *   This mechanism is intended to allow for flexible configuration of 
	 *   the state propagation. 
	 *   Parent Relevancy sets a maximum state that the parent will become due to
	 *   this child. A relevancy of "HIDDEN" means the parent will never change
	 *   state because of this child. 
	 *   The default relevancy of "SUSPICIOUS" means that the parent will change to
	 *   at most "SUSPICIOUS" level if the child goes to SUSPICIOUS or higher.
	 *   A setting of FAILED means that the parent will be marked as FAILED if this
	 *   child goes into FAILED state.
	 * 
	 *   Note that this is mostly for convenient trickling of state up through the tree, 
	 *   to notify the user of lower components being in a degraded state. It is 
	 *   encouraged to implement a proper child state dependency in the same 
	 *   function that runs "set_state".
	 * 
	 * @param relevancy 
	 */
	void set_parent_relevancy(component_state_t relevancy);

	/**
	 * @brief Get the child component object
	 * @details Returns the first child object of this component, or nullptr.
	 *   Can be used to iterate over all children by calling get_next() on the
	 *   child.
	 * @return Component* 
	 */
	Component * get_child_component();
	/**
	 * @brief Look for a specific child component.
	 * @details This function will iterate the child list until it finds a child with 
	 *   the given name, and will return a pointer to it. If no child with matching 
	 *   name is found, nullptr is returned.
	 * 
	 *   Note that special syntax is used here. If a dot (.) is found, it is treated
	 *   as path separator. This function will then descend the component tree by 
	 *   searching for the given child name up to the dot, then continue searching
	 *   *its* children.
	 *   
	 *   Example: get_child_component("ADCS.BANK_A.CHANNEL1") will look for "ADCS" in the
	 *   root component, then for "BANK_A" in the "ADCS" component, then "CHANNEL1" in BANK_A
	 * 
	 * @param name Name or path, dot-separated, of the component to look for
	 * @return Component* Pointer to the found child, or nullptr if nothing was found
	 */
	Component * get_child_component(const char * name);
	/**
	 * @brief Get the next child object
	 * @details This function can be used together with get_child_component() to iterate all children of a 
	 *   component. It will return the next child in the list, or nullptr if the end of the list was reached.
	 * @return Component* Pointer to the next child of the list, or nullptr if at the end.
	 */
	Component * get_next();
	//! @brief Returns pointer to the parent object, or nullptr if Component is not yet attached
	Component * get_parent();

	//! @brief Return the current state of the component (worse of component_state and child_state)
	component_state_t get_state();
	//! @brief Get string representation of the current state
	const char * get_state_name();

	/**
	 * @brief Returns the user supplies state message (or default)
	 * @details This function will return the pointer to the string supplied to set_state(),
	 *   or will return a default representation. If a child component is responsible for the
	 *   current state, then "CHECK CHILD" will be returned. Else, a string matching the 
	 *   name of the current state (get_state_name()) is returned
	 * @return const char* 
	 */
	const char * get_state_message();
	//! @brief Get optional string colorization to decorate Terminal output
	const char * get_colorization();

	///@{
	/// @warning These functions are unimplemented and speculative, and might be replaced in the future.
	///  They are listed here as written-down ideas only.
	virtual bool set_parameter(const char * key, const char * data);
	virtual bool get_parameter(const char * key, char * buffer, size_t max_len);
	virtual const char * get_parameter_name(int i);
	///@}
};

}
