/**
 * @author David Bailey (davidbailey.2889@gmail.com) 
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#include "base64.h"

static const char encoding_table[] = {
                                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'
};

void base64_encode(const void * raw_data, char * buffer, size_t num) {
    if(raw_data == 0) {
        buffer[0] = 0;
        return;
    }

    uint8_t * data = (uint8_t *)(raw_data);

    int i=0;
    // Handle full triplets first
    for (i = 0; (i+2) < num; i+=3) {
        uint32_t triple = ((uint32_t)data[i] << 0x10) | ((uint32_t)data[i+1] << 0x08) | (uint32_t)data[i+2];

        *(buffer++) = encoding_table[(triple >> 3 * 6) & 0x3F];
        *(buffer++) = encoding_table[(triple >> 2 * 6) & 0x3F];
        *(buffer++) = encoding_table[(triple >> 1 * 6) & 0x3F];
        *(buffer++) = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    // Handle a single incomplete triplet
    if(i != num) {

    	uint32_t triple = ((uint32_t)data[i] << 0x10);
    	if((i+2) == num)
    		triple |= ((uint32_t)data[i+1] << 0x08);

        *(buffer++) = encoding_table[(triple >> 3 * 6) & 0x3F];
        *(buffer++) = encoding_table[(triple >> 2 * 6) & 0x3F];
        *(buffer++) = ((i+2) == num) ? encoding_table[(triple >> 1 * 6) & 0x3F] : '=';
        *(buffer++) = '=';
    }

    *buffer = 0;
}
