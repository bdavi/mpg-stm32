/**
 * @file base64.h
 * @author David Bailey (davidbailey.2889@gmail.com)
 * @brief Simple Base64 encoder code
 * @version 0.1
 * @date 2022-06-16
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#pragma once

#include <stddef.h>
#include <stdint.h>

/**
 * @brief Encode the given data to Base64
 * @details This function will transform the given data
 *   into an ASCII-Representable string, written into buffer.
 *   The string is a valid null-terminated string.
 *  
 *   Base64 encoding takes three input bytes, and turns them into four
 *   output bytes, using only characters a-z, A-Z, 0-9,'+' and '/'.
 *   It is thusly excellently suited to wrap arbitrary binary data and
 *   send it through any channel that can handle printable characters.
 * 
 *   Note: It is safe to call this encoding function on partial chunks
 *   of data to split a long buffer into smaller, easier to handle ones. 
 *   Please note however that it is recommended to use partial chunks
 *   in multiples of three, to avoid padding '='
 * 
 * @warning The buffer must be AT LEAST 4/3 * num + 3 characters in length!
 *   No checks are done on "buffer". Base64 uses four bytes of output per 
 *   three input bytes. Additionally, padding '=' and a null-terminator 
 *   are needed!
 * 
 * @param data Arbitrary input data to be converted to Base64
 * @param buffer The output buffer to write into. Must be at least
 *   4/3*num + 3 characters long!
 * @param num Number of data-bytes to encode
 */
void base64_encode(const void * data, char * buffer, size_t num);