/**
 * @author David Bailey (davidbailey.2889@gmail.com) 
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#include "color.h"
#include <algorithm>

#define IND_BOUND_COLOR(v) std::min<int32_t>(IND_COLOR_MAX, std::max<int32_t>(0, v))
#define IND_U8_TO_RAW_C(v) uint8_t(v) * uint16_t(255)
#define IND_RAW_TO_U8(v)   uint8_t(v/255)

#define IND_MULTIPLY_RAW(a, b) IND_BOUND_COLOR((uint32_t(a) * uint32_t(b)) / IND_COLOR_MAX)

namespace Indicators {

color_t color_t::from_hsv(int16_t H, uint8_t S, uint8_t V) {
	H %= 360;
	if(H < 0)
		H += 360;

	uint16_t h = (H/60);
	uint16_t f = ((uint32_t(H)*255)/60) % 255;

	uint16_t p = uint16_t(V) * (255 - S);
	uint16_t q = uint16_t(V) * (255 - (S*f)/255);
	uint16_t t = uint16_t(V) * (255 - (S*(255 - f))/255);

	color_t oC = color_t();

	switch(h) {
	default:oC.r = V*255; oC.g = t; oC.b = p; break;
	case 1: oC.r = q; oC.g = V*255; oC.b = p; break;
	case 2: oC.r = p; oC.g = V*255; oC.b = t; break;
	case 3: oC.r = p; oC.g = q; oC.b = V*255; break;
	case 4: oC.r = t; oC.g = p; oC.b = V*255; break;
	case 5: oC.r = V*255; oC.g = p; oC.b = q; break;
	}

	return oC;
}

color_t color_t::from_u32(uint32_t code) {
    color_t out_color;

    uint8_t *colorPart = (uint8_t *)&code;
	
    out_color.r = IND_U8_TO_RAW_C(colorPart[2]);
	out_color.g = IND_U8_TO_RAW_C(colorPart[1]);
	out_color.b = IND_U8_TO_RAW_C(colorPart[0]);

	return out_color;
}

color_t::color_t() 
    : r(0), g(0), b(0), alpha(IND_COLOR_MAX) {

}

color_t color_t::pow_adjust() const {
    color_t out;

    out.r = IND_MULTIPLY_RAW(r, r);
    out.g = IND_MULTIPLY_RAW(g, g);
    out.b = IND_MULTIPLY_RAW(b, b);

    return out;
}

color_t & color_t::dim(uint16_t amount) {
    amount = IND_BOUND_COLOR(amount);

    r = IND_MULTIPLY_RAW(amount, r);
    g = IND_MULTIPLY_RAW(amount, g);
    b = IND_MULTIPLY_RAW(amount, b);

    return *this;
}

#define MERGE_OVERLAY(code) (this->code) = (uint32_t(this->code)*own_transmission_p + uint32_t(top.code)*(65025-own_transmission_p))/(65025)
color_t & color_t::overlay(const color_t &top, uint16_t alpha) {

	uint16_t total_alpha_top = IND_MULTIPLY_RAW(top.alpha, alpha);

	uint32_t own_transmission = IND_MULTIPLY_RAW(this->alpha, (IND_COLOR_MAX - total_alpha_top));
	uint32_t own_transmission_p = 0;
	if(own_transmission != 0)
		own_transmission_p = (IND_COLOR_MAX * own_transmission) / (own_transmission + total_alpha_top);

	MERGE_OVERLAY(r);
	MERGE_OVERLAY(g);
	MERGE_OVERLAY(b);

	this->alpha = IND_COLOR_MAX - IND_MULTIPLY_RAW(IND_COLOR_MAX - this->alpha, IND_COLOR_MAX - total_alpha_top);

	return *this;
}

}
