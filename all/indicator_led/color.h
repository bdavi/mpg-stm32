/**
 * @file color.h
 * @author David Bailey (davidbailey.2889@gmail.com)
 * @brief WS2812 RGB colour handling code
 * @version 0.1
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#pragma once

#include <stdint.h>
#include <array>

#define IND_COLOR_MAX (255*255)

enum Material : uint32_t {
	BLACK 	= 0x000000,
	RED 	= 0xF42316,
	PINK	= 0xE91E63,
	PURPLE	= 0x9C27B0,
	DEEP_PURPLE = 0x6A3AB7,
	INDIGO	= 0x3F51B5,
	BLUE	= 0x0546FF,
	CYAN	= 0x00CCE4,
	GREEN	= 0x20DF2C,
	LIME	= 0xCCE210,
	YELLOW	= 0xEBEB00,
	AMBER	= 0xFFC007,
	ORANGE	= 0xFF9800,
	DEEP_ORANGE = 0xFF4700
};

namespace Indicators {

class color_t {
public:
    uint16_t r;
    uint16_t g; 
    uint16_t b;

    uint16_t alpha;

	static color_t from_hsv(int16_t H, uint8_t S = 255, uint8_t V = 255);
    static color_t from_u32(uint32_t code);


    color_t();
    
    color_t pow_adjust() const;

    color_t & dim(uint16_t amount);
    color_t & dimf(float amount)  { return dim(IND_COLOR_MAX * amount); };
    
    color_t & overlay(const color_t &other, uint16_t alpha = IND_COLOR_MAX);
};


}
