/**
 * @author David Bailey (davidbailey.2889@gmail.com) 
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#include "led.h"

#include <FreeRTOS.h>
#include <task.h>

namespace Indicators {

uint32_t get_led_time() {
    return (xTaskGetTickCount() * portTICK_PERIOD_MS);
}

LED::LED() 
    : color(), target_color(), mode(NONE) 
    {
}

LED & LED::operator=(const color_t & color) {
    target_color = color;
    return *this;
}

void LED::draw_tick() {
    color_t next_color = target_color;

    switch(mode) {
        default:
            color.overlay(target_color, 20000);
        break;
        case HARD_ON:
        	color = target_color;
        break;
        case RAINBOW:
            color.overlay(color_t::from_hsv(get_led_time()/4), 10000);
        break;
        case STANDBY:
        	next_color.dim((get_led_time() % 5000) < 100 ? 25000 : 8000);
        	color = next_color;
        break;
        case IDLE:
        	next_color.dim((get_led_time() % 3000) < 600 ? 40000 : 20000);
        	color.overlay(next_color, 15000);
        break;
        case FLICKER:
            next_color.dim((((get_led_time() / 50) & 1) == 0) ? 40000 : 0);
            color.overlay(next_color, 30000);
        break;
        case WARN:
            next_color.dim((((get_led_time() / 500) & 1) == 0) ? IND_COLOR_MAX : 20000);
            color.overlay(next_color, 30000);
        break;
        case ALERT:
            next_color.dim((0b0010100101 & 1<<((get_led_time() / 50) % 10)) ? IND_COLOR_MAX : 0);
            color = next_color;
        break;
    }
}

#define DUMP_COLORPART(u)\
    do {\
    uint8_t uData = uint8_t(u/255);\
    for(int i=0; i<4; i++) { *target = 0b00010001;\
    if((uData&0b10000000) != 0) *target |= 0b01100000;\
    if((uData&0b01000000) != 0) *target |= 0b00000110;\
    uData <<= 2;\
    target++;\
}\
} while(0)

void LED::write_rgb_data_to(uint8_t * target) {
    const auto adjusted_color = color.pow_adjust();

    DUMP_COLORPART(adjusted_color.r);
    DUMP_COLORPART(adjusted_color.g);
    DUMP_COLORPART(adjusted_color.b);
}

}
