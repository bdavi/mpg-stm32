/**
 * @file led.h
 * @author David Bailey (davidbailey.2889@gmail.com)
 * @brief Indicator pattern handling (flashing LEDs etc)
 * @version 0.1
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#pragma once

#include <stdint.h>
#include <array>

#include "color.h"

namespace Indicators {

enum led_mode_t {
    NONE,
	HARD_ON,
	RAINBOW,
    STANDBY,
	IDLE,
    FLICKER,
	WARN,
	ALERT,
};  

class LED {
public:
    color_t color;
    color_t target_color;

    led_mode_t mode;

    LED();

    LED & operator=(const color_t &color);

    void draw_tick();
    
    void write_rgb_data_to(uint8_t * data);
};

}
