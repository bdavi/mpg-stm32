/**
 * @author David Bailey (davidbailey.2889@gmail.com) 
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#include "log.h"

#include <FreeRTOS.h>
#include <semphr.h>

#include "../encodings/base64.h"

#include "log_error_recovery.h"

#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

__attribute__((unused)) static const char * log_colorizations[] = {
    "V ",
    "D ",
    STR_COLOR_GREEN "I ",
    STR_COLOR_YELLOW "W ",
    STR_COLOR_RED "E ",
    STR_COLOR_REDB "F "
};

char log_printf_buffer[LOG_PRINTF_BUFFER_LEN] = {};

char log_write_buffer[LOG_WRITE_BUFFER_LEN] = {};
uint32_t log_buffer_canary_byte = 0x12345678;

volatile int log_write_buffer_txpos = 0;
volatile int log_write_buffer_rxpos = 0;

StaticSemaphore_t log_buffer_mutex_buffer;
xSemaphoreHandle log_buffer_mutex = 0;


StaticSemaphore_t log_buffer_write_mutex_buffer;
xSemaphoreHandle log_buffer_write_mutex = 0;

TaskHandle_t waiting_on_send_task  = 0;
TaskHandle_t waiting_on_write_task = 0;

void log_init() {
	// Ignore double-initializations.
	if(log_buffer_mutex || log_buffer_write_mutex)
		return;

	log_buffer_mutex = xSemaphoreCreateMutexStatic(&log_buffer_mutex_buffer);
	log_buffer_write_mutex = xSemaphoreCreateMutexStatic(&log_buffer_write_mutex_buffer);

	log_puts("\n\n\n\x1b[33m--------------\nSYS: REBOOTING\n--------------\x1b[0m\n");

	log_error_recovery_init();
	log_puts("\n");

	LOGI("SYS", "Logging started up!");
}

int log_get_bytes_free() {
	int local_write_buffer_txpos = 0;
	int local_write_buffer_rxpos = 0;

	portENTER_CRITICAL();
	local_write_buffer_txpos = log_write_buffer_txpos;
	local_write_buffer_rxpos = log_write_buffer_rxpos;
	portEXIT_CRITICAL();

	if(local_write_buffer_txpos >= local_write_buffer_rxpos)
		return LOG_WRITE_BUFFER_LEN - 1 - local_write_buffer_txpos + local_write_buffer_rxpos;
	else
		return local_write_buffer_rxpos - local_write_buffer_txpos - 1;
}

void log_notify_data_available() {
	if(waiting_on_send_task)
		xTaskNotifyGive(waiting_on_send_task);
}

void log_buffer_write_lock() {
	if(log_buffer_write_mutex) {
		xSemaphoreTake(log_buffer_write_mutex, portMAX_DELAY);
	}
}
void log_buffer_write_unlock() {
	if(log_buffer_write_mutex) {
		xSemaphoreGive(log_buffer_write_mutex);
	}

	log_notify_data_available();
}

void log_buffer_lock() {}
void log_buffer_unlock() {}

void log_puts_nowlock(const char *c) {

	log_buffer_lock();

	int len = strlen(c);

	// The string will be copied over in chunks of maximum possible length,
	// i.e. from TXPOS to the end of the buffer, from TXPOS to RXPOS, 
	// or the string length.
	while(len > 0) {
		int chunk_len = 0;

		portENTER_CRITICAL();
		if(log_write_buffer_txpos >= log_write_buffer_rxpos) {
			chunk_len = LOG_WRITE_BUFFER_LEN - log_write_buffer_txpos;

			// This needs to happen to keep the txpos from rolling over into the rxpos,
			// which would make the buffer look empty (even though it now is full)
			if(log_write_buffer_rxpos == 0)
				chunk_len -= 1;
		}
		else {
			// Always leave one space between RX and TX. RX == TX means empty buffer, not full
			chunk_len = log_write_buffer_rxpos - log_write_buffer_txpos - 1;
		}
		portEXIT_CRITICAL();

		if(chunk_len > len)
			chunk_len = len;

		// A chunk length of 0 despite len > 0 indicates that the buffer
		// is full, and we need to wait here.
		if(chunk_len <= 0) {
			// Unlock access so the sending task can use buffer
			log_buffer_unlock();

			// Tell the sending task that there's fresh data
			if(waiting_on_send_task) {
				xTaskNotifyGive(waiting_on_send_task);
			}

			// Then set this task up to wait for sending to finish
			waiting_on_write_task = xTaskGetCurrentTaskHandle();
			xTaskNotifyWait(0, 0, 0, 20);
			
			log_buffer_lock();

			// Re-check the rxpos pointer and recalculate chunk length.
			continue;
		}

		// Copy the chunk of data into the buffer, and advance
		// the txpos pointer.
		memcpy(log_write_buffer + log_write_buffer_txpos, c, chunk_len);

		c += chunk_len;
		len -= chunk_len;

		portENTER_CRITICAL();
		log_write_buffer_txpos += chunk_len;

		if(log_write_buffer_txpos >= LOG_WRITE_BUFFER_LEN)
			log_write_buffer_txpos -= LOG_WRITE_BUFFER_LEN;
		portEXIT_CRITICAL();
	}

	waiting_on_write_task = 0;

	log_buffer_unlock();

	if(log_buffer_canary_byte != 0x12345678) {
		asm("bkpt");
		assert(0);
	}
}

void log_puts(const char *c) {
	log_buffer_write_lock();
	log_puts_nowlock(c);
	log_buffer_write_unlock();

	log_notify_data_available();
}

void log_printf_nowlock(const char * msg, ...) {
	va_list args;
	va_start (args, msg);

	vsnprintf(log_printf_buffer, sizeof(log_printf_buffer)-1, msg, args);
	log_puts_nowlock(log_printf_buffer);

	va_end (args);
}

void log_printf(const char * msg, ...) {
  va_list args;
  va_start (args, msg);

  // Locking is necessary to avoid mutual access to the log printf buffer.
  log_buffer_write_lock();

  vsnprintf(log_printf_buffer, sizeof(log_printf_buffer)-1, msg, args);
  log_puts_nowlock(log_printf_buffer);

  log_buffer_write_unlock();

  va_end (args);
}

void log_message(log_level_t level, const char *tag, const char *msg, ...) {
	if(level < LOG_VERBOSE || level >= LOG_MAX)
		return;
		
	// Implement dropping of debug, verbose and info log messages at varying output buffer 
	// capacity. This shall prevent code from slowing down or locking up at high log throughput, 
	// at the expense of less reliable messages. 
	// Higher-level warnings are never dropped
	switch(level) {
		default: break;
		case LOG_VERBOSE:
		case LOG_DEBUG:
			if(log_get_bytes_free() < LOG_WRITE_BUFFER_LEN/2)
				return;
		break;

		case LOG_INFO:
			if(log_get_bytes_free() < (LOG_WRITE_BUFFER_LEN*1)/4)
				return;
		break;
	}


	log_buffer_write_lock();

//	uint64_t log_uptime = xTaskGetTickCount();
//	int log_days   =  (log_uptime  / (1000*60*60*24));
//	int log_hours  =  (log_uptime  / (1000*60*60)) % 24;
//	int log_minutes = (log_uptime / (1000*60)) % 60;
//	int log_seconds = (log_uptime / 1000) % 60;
//	int log_ms      = (log_uptime) % 1000;

	snprintf(log_printf_buffer, sizeof(log_printf_buffer)-1, "%s[%010lu] %8s: ",
			log_colorizations[level], xTaskGetTickCount(),
			tag);

	log_puts_nowlock(log_printf_buffer);

	va_list args;
	va_start (args, msg);
	vsnprintf(log_printf_buffer, sizeof(log_printf_buffer)-1, msg, args);
	va_end (args); 
	
	log_puts_nowlock(log_printf_buffer);

#ifdef LOG_NO_COLOURS
	log_puts_nowlock("\n");
#else
	log_puts_nowlock("\x1b[0m\n");
#endif

	log_buffer_write_unlock();
}

void log_dump_data_base64_nowrap_nowlock(const char * tag, const void * data, size_t num) {
	while(num > 0) {

		if(log_get_bytes_free() < ((LOG_WRITE_BUFFER_LEN)/2)) {

			log_buffer_write_unlock();

			waiting_on_write_task = xTaskGetCurrentTaskHandle();
			ulTaskNotifyTake(1, 10);

			log_buffer_write_lock();

			continue;
		}

		size_t chunk_num = num;
		if(chunk_num > 96)
			chunk_num = 96;

		snprintf(log_printf_buffer, 16, "%s: ", tag);
		base64_encode(data, log_printf_buffer + strlen(log_printf_buffer), chunk_num);

		size_t log_pos = strlen(log_printf_buffer);
		log_printf_buffer[log_pos] = '\n';
		log_printf_buffer[log_pos+1] = 0;

		num -= chunk_num;
		data += chunk_num;

		log_puts_nowlock(log_printf_buffer);

		// log_buffer_write_unlock();
	}
}

void log_dump_data_base64(const char * tag, const void * data, size_t num) {
	log_printf("%s: START BASE64\n", tag);
	log_buffer_write_lock();

	log_dump_data_base64_nowrap_nowlock(tag, data, num);

	log_buffer_write_unlock();
	log_printf("%s: DONE\n", tag);
}

void log_take_buffer(size_t * length, const char **ptr) {
	if(length == 0)
		return;
	if(*length == 0)
		return;
	if(ptr == 0)
		return;

	waiting_on_send_task = xTaskGetCurrentTaskHandle();

	// No data is in the buffer at the moment, as such we will wait for
	// data to arrive. log_puts will send a task notification to wake us up
	while(log_write_buffer_txpos == log_write_buffer_rxpos) {
		ulTaskNotifyTake(1, 1000);
	}

	log_buffer_lock();

	int max_rx_len = 0;
	*ptr = log_write_buffer + log_write_buffer_rxpos;

	//  The log write buffer is a circular buffer, meaning that the write position
	// could have wrapped around whereas the read position has not.
	// In this case, we will return a string from the read position to the end
	// of the buffer and wrap the rxposition to 0
	portENTER_CRITICAL();
	if(log_write_buffer_rxpos > log_write_buffer_txpos) {
		max_rx_len = LOG_WRITE_BUFFER_LEN - log_write_buffer_rxpos;
	}
	// In the other case, we can only send up to txpos
	else {
		max_rx_len = log_write_buffer_txpos - log_write_buffer_rxpos;
	}
	portEXIT_CRITICAL();

	if(*length > max_rx_len)
		*length = max_rx_len;

	log_buffer_unlock();
}

void log_free_buffer(size_t count) {
	log_buffer_lock();

	// Advance the write buffer

	portENTER_CRITICAL();
	log_write_buffer_rxpos += count;
	if(log_write_buffer_rxpos >= LOG_WRITE_BUFFER_LEN)
		log_write_buffer_rxpos = 0;
	portEXIT_CRITICAL();

	if(waiting_on_write_task) {
		xTaskNotifyGive(waiting_on_write_task);
	}
}
