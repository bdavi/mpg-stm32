/**
 * @file all/log/log.h
 * @author David Bailey (davidbailey.2889@gmail.com)
 * @brief Unified STM32 logging library code
 * @version 0.1
 * @date 2022-05-02
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 * @details This library is used to provide a unified output stream interface.
 *   This stream can, and should be, used for any human readable output. Most commonly
 *   this will be log messages, though it is encouraged to route other outputs through
 *   this channel when applicable.
 * 
 *   The log messages have taken heavy inspiration from the ESP_LOG system, and feature a similar
 *   pattern.
 *   Log messages can be sent by using the LOGx macros. These feature a lightweight compile-time disable mechanism
 *   by allowing the user to define the LOG_LEVEL macro before including this file. Any LOGx macros with lesser log 
 *   level will be optimized out by the compiler. It is thusly easy, and encouraged, to leave any debug statements
 *   in.
 *   Additionally, any debug and verbose statement will NOT be written to console if the output buffer is more 
 *   than half full. Any info message will not be written if buffer is more than 3/4 full. Warning and Error
 *   messages are ALWAYS written. This is intended to prevent buffer congestion and slowdown of the system
 *   due to high log message throughput.
 * 
 *   When using this library to send data other than log messages, the following must be taken into account to
 *   remain compatible:
 *     - Any ASCII colorization shall be stripped by the parser
 *     - Any data sent should be ASCII displayable. It is encouraged, but not necessary, to format data
 *       in a way that displays nicely in a standard terminal.
 *     - Data should be formatted in lines. Each line MUST start with a beginning tag closed 
 *       by a colon (:) to identify its purpose. 
 *       "V", "D", "I", "W", "E", "F" are reserved for log messages. Valid tags consist of
 *       letters, numbers, "_", "/". Example "ADC2_Data", "STATUS/VMON"
 *       A newline indicates the end of a data packet. If data is to be spread across multiple lines, it is
 *       encouraged to use a single line containing the word "DONE" to indicate completion.
 * 
 *   An example custom data line may be:
 *   @code
 *      I [00123123] Thing: Just a regular log line here!
 *      ADC_DATA: [0, 1, 2, 3, 5, 12
 *      ADC_DATA:  89, 213, 524 ]
 *      ADC_DATA: DONE
 *   @endcode
 * 
 *   Using this logging library requires FreeRTOS as a prerequisite, as the output buffer uses mutex synchronisation.
 *   log_init() MUST be called to set up the mutexes, preferably before starting the scheduler.
 *   Additionally, the user must provide the actual output mechanisms. 
 *   A dedicated log output task should be set up, and log_take_buffer() and log_free_buffer() should be used
 *   to send out the buffer. This allows great flexibility in the number and type of output channels, and
 *   allows easy rerouting of log messages through a great number of channels.
 * 
 *   A simple DMA UART Transfer can look as follows:
 *   @code
volatile bool uart_busy = false;
void uart_log_task(void const * argument)
{

  for(;;)
  {
    const char * uart_buffer = 0;
    size_t uart_len = 256;

    log_take_buffer(&uart_len, &uart_buffer);

    HAL_UART_Transmit_DMA(&huart3, uart_buffer, uart_len);
    uart_busy = true;
    while(uart_busy)
        xTaskNotifyWait(0, 0, 0, portMAX_DELAY);

    log_free_buffer(uart_len);
  }
}
 *   @endcode
 *   With the UART DMA transfer complete callback being used to set the uart_busy flag back to false and send a task
 *   notification to the UART task.
 * 
 * @pre FreeRTOS is required as this code uses mutex synchronization on the UART buffer as well as blocking transmitting
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once

#include "main.h"

#include <stddef.h>

#include "log_utils.h"

#ifndef LOG_PRINTF_BUFFER_LEN
#define LOG_PRINTF_BUFFER_LEN 256
#endif

#ifndef LOG_WRITE_BUFFER_LEN
#define LOG_WRITE_BUFFER_LEN 2048
#endif

#ifndef LOG_LEVEL
#define LOG_LEVEL LOG_INFO
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    LOG_VERBOSE,
    LOG_DEBUG,
    LOG_INFO,
    LOG_WARNING,
    LOG_ERROR,
    LOG_FATAL,
    LOG_MAX
} log_level_t;

#ifdef LOG_NO_COLOURS

#define STR_COLOR_GREEN  ""
#define STR_COLOR_YELLOW ""
#define STR_COLOR_PURPLE ""
#define STR_COLOR_RED    ""
#define STR_COLOR_REDB   ""
#define STR_COLOR_RESET  ""

#else

#define STR_COLOR_GREEN  "\x1b[32m"
#define STR_COLOR_YELLOW "\x1b[33m"
#define STR_COLOR_PURPLE "\x1b[35m"
#define STR_COLOR_RED    "\x1b[31m"
#define STR_COLOR_REDB   "\x1b[31;1m"
#define STR_COLOR_RESET  "\x1b[0m"

#endif

/**
 * @brief Initialize the log messaging system. Must be called before all other uses of log system.
 */
void log_init();

/**
 * @brief Put a string into the log write buffer
 * @details This function will copy a string into the logging buffer.
 *  Do note that it may not return immediately, but instead wait on a FreeRTOS lock or for the buffer to empty sufficiently.
 *  After it returns, the string can be discarded
 */
void log_puts(const char * str);

/**
 * @brief Printf a string into the log buffer
 * @details This function will print a string into the logging buffer. 
 *  Please note that it is limited to LOG_PRINTF_BUFFER_LEN bytes (default 255) write due to the internally used buffer. 
 *  Just like log_puts, it will wait until the entire string has been written to the output buffer before returning, and 
 *  may not return immediately.
 */
void log_printf(const char *msg, ...);

/**
 * @brief Send a message at given log level.
 * @details This function will format the given message into a log line.
 *   Log lines are colorized, start with a level indicator (V, D, I, W, E or F), 
 *   contain the FreeRTOS tick they were issued at, a tag of the log source, and otherwise behave like 
 *   printf, allowing easy insertion of parameters about what was logged.
 * 
 *   Please note that it is encouraged to use the LOGx macros instead. These allow for compile-time optimizations
 *   of log messages by using the LOG_LEVEL macro. This means that lower-level messages are not present in the compiled
 *   binary unless necessary, saving the overhead of the "if" check.
 * 
 * @param level The log level to use.
 * @param tag   The tag of the code unit emitting the log line
 * @param msg   The message to be sent. Can contain all printf statements. %f should be avoided, as the
 *    floating point printf implementation seems buggy and requires a lot of stack. The message is 
 *    limited to LOG_PRINTF_BUFFER_LEN (default 255) bytes in length.
 */
void log_message(log_level_t level, const char *tag, const char *msg, ...);


#define LOG_WITH_LEVEL(LEVEL, TAG, MSG, ...) do { \
		if(LOG_LEVEL <=  LEVEL) { log_message(LEVEL, TAG, MSG, ##__VA_ARGS__); } \
	} while(0)

#define LOGV(TAG, MSG, ...) LOG_WITH_LEVEL(LOG_VERBOSE, TAG, MSG, ##__VA_ARGS__)
#define LOGD(TAG, MSG, ...) LOG_WITH_LEVEL(LOG_DEBUG, TAG, MSG, ##__VA_ARGS__)
#define LOGI(TAG, MSG, ...) LOG_WITH_LEVEL(LOG_INFO, TAG, MSG, ##__VA_ARGS__)
#define LOGW(TAG, MSG, ...) LOG_WITH_LEVEL(LOG_WARNING, TAG, MSG, ##__VA_ARGS__)
#define LOGE(TAG, MSG, ...) LOG_WITH_LEVEL(LOG_ERROR, TAG, MSG, ##__VA_ARGS__)
#define LOGF(TAG, MSG, ...) LOG_WITH_LEVEL(LOG_FATAL, TAG, MSG, ##__VA_ARGS__)

/**
 * @brief Dump a large buffer using Base64 encoding
 * @details This function will send an arbitrarily large chunk of data
 *   through the log system using Base64 encoding. Lines will be preceded
 *   with "tag". A starting message is used ("TAG: START BASE64"), followed by
 *   lines of Base64-encoded data. A finishing tag ("TAG: DONE") is sent
 *   after all data lines have been sent.
 *   
 *   This function will block until all data has been sent!
 * 
 *   @note This function will only fill the buffer to 1/2 full at most, to 
 *    ensure that other log messages can still be sent. The decoder must
 *    appropriately filter out lines not starting with TAG when 
 *    collecting Base64 data!
 */
void log_dump_data_base64(const char * tag, const void * data, size_t num);
void log_dump_data_base64_nowrap_nowlock(const char *tag, const void *data, size_t num);

/**
 * @brief Lock the write buffer
 * @details This function should be used if the user wishes to dump a larger block of custom data
 *   onto the log buffer than what log_printf can achieve. This will acquire the mutex lock
 *   of the write buffer, and allow continuous access to it.
 * 
 *   log_buffer_write_release() MUST be called! The user MUST use log_puts_nowlock() and
 *   log_printf_nowlock(), and MUST NOT use the regular log_puts() and log_printf()
 * 
 * @sa log_buffer_write_release()
 */
void log_buffer_write_lock();
/**
 * @brief Release the log buffer mutex
 */
void log_buffer_write_unlock();

/**
 * @brief Put a null-terminated string of data onto the buffer without
 *  acquiring the buffer lock.
 * 
 * @sa log_buffer_write_lock
 */
void log_puts_nowlock(const char *str);
/**
 * @brief printf data onto the log buffer without acquiring the
 *   buffer lock.
 * 
 * @sa log_buffer_write_lock
 */
void log_printf_nowlock(const char *msg, ...);

/**
 * @brief Take a section of buffer for transmitting
 * @details This function will check if there is data in the write buffer that needs to be sent out.
 *   If there is no data, it will block and wait for something to be written into 
 *   the buffer.
 *   If data is present, it will write a pointer to the start of the data into buffer, and 
 *   write the lenght of data to be sent into length. The initial value of lenght will 
 *   determine the maximum length returned.
 * 
 *   The string pointer to by buffer will remain valid in the Log Write Buffer. 
 *   log_free_buffer() MUST be called to actually free this section of buffer.
 * 
 * @sa log_free_buffer
 *  
 * @param length Pointer to a size_t to write the available length into.
 *    Initial value will specify max length returned.
 * @param buffer Pointer to a Pointer of type const char. If lenght returns >0 
 *    then this will be set to the start of the data to be sent out. The data 
 *    may not be null-terminated! 
 *    Data remains valid until log_free_buffer() is called!
 */
void log_take_buffer(size_t *length, const char **buffer);

/**
 * @brief Free length bytes in the log buffer
 * @details This function MUST be called after log_take_buffer and 
 *  the log sending has completed, in order to free the log 
 *  buffer space for further writing.
 * 
 * @sa log_take_buffer
 * @param length Number of bytes to free
 */
void log_free_buffer(size_t length);

#ifdef __cplusplus
}
#endif
