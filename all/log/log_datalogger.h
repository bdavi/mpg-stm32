/**
 * @file log_datalogger.h
 *
 *  Created on: 19 Oct 2022
 * @author: David Bailey
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef ALL_LOG_LOG_DATALOGGER_H_
#define ALL_LOG_LOG_DATALOGGER_H_

#include <stddef.h>
#include <stdint.h>

#include <FreeRTOS.h>
#include <task.h>

namespace Log {

template <class T, size_t buffer_len, int type_count = 1>
class Datalogger {
private:
#pragma pack(1)
	struct data_store_t {
		uint16_t chomp_timestamps[buffer_len];
		T datapoints[type_count][buffer_len];
	};

	typedef T datapoint_t[type_count];
#pragma pack(0)

	data_store_t store;

	volatile size_t buffer_write_pos = 0;
	size_t buffer_read_pos  = 0;

	TickType_t last_sent_tick;

public:
	const char * tag;

	Datalogger(const char * channel_tag) : store(),
			buffer_write_pos(0), buffer_read_pos(0),
			last_sent_tick(0), tag(channel_tag) {

	}

	void feed(const datapoint_t & point, TickType_t time) {
		store.chomp_timestamps[buffer_write_pos] = (time) & 0xFFFF;
		for(int i=0; i<type_count; i++)
			store.datapoints[i][buffer_write_pos] = point[i];

		buffer_write_pos = (buffer_write_pos+1) % buffer_len;

		if(buffer_write_pos == buffer_read_pos)
			buffer_read_pos = (buffer_read_pos+1) % buffer_len;
	}
	void feed(const datapoint_t & point) {
		feed(point, xTaskGetTickCountFromISR());
	}

	size_t free_size() {
		if(buffer_write_pos >= buffer_read_pos)
			return buffer_len - buffer_write_pos + buffer_read_pos - 1;
		return buffer_read_pos - buffer_write_pos - 1;
	}

	bool should_send_logs() {
		if((xTaskGetTickCount()-last_sent_tick) >= 10000)
			return true;

		if(free_size() < buffer_len/2)
			return true;

		return false;
	}

	void send_logs() {
		if(!should_send_logs())
			return;

		last_sent_tick = xTaskGetTickCount();

		size_t send_up_to = buffer_write_pos;

		if(send_up_to < buffer_read_pos)
			send_up_to = buffer_len;

		int n_points = send_up_to - buffer_read_pos;

		log_buffer_write_lock();
		log_printf_nowlock("%s: START DATAPOINTS %lu %d %d\n", tag, last_sent_tick, n_points, type_count);

		log_dump_data_base64_nowrap_nowlock(tag, &(store.chomp_timestamps[buffer_read_pos]), n_points * sizeof(uint16_t));

		log_printf_nowlock("%s: ---\n", tag);

		for(int i=0; i<type_count; i++) {
			log_dump_data_base64_nowrap_nowlock(tag, &(store.datapoints[i][buffer_read_pos]), n_points * sizeof(T));
			if(i < type_count-1)
				log_printf_nowlock("%s: ---\n", tag);
		}

		log_printf_nowlock("%s: DONE\n", tag);
		log_buffer_write_unlock();

		buffer_read_pos = send_up_to;
		if(buffer_read_pos == buffer_len)
			buffer_read_pos = 0;
	}

};

}


#endif /* ALL_LOG_LOG_DATALOGGER_H_ */
