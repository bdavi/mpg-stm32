/**
 * @author David Bailey (davidbailey.2889@gmail.com) 
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#include "log_error_recovery.h"
#include "string.h"

NOINIT_ATTR error_recovery_section_t log_error_recovery_data;

void log_error_recovery_init() {
	if(log_error_recovery_data.magic_number == LOG_ERROR_RECOVERY_MAGIC_NUMBER) {
		LOGI("SYS", "Recovery data present!");

		log_message(log_error_recovery_data.error_level, "SYS", "Error recovery message: %s",
				log_error_recovery_data.error_msg_buffer);
	}

	memset(&log_error_recovery_data, 0, sizeof(log_error_recovery_data));
}

void log_error_mark_error(log_level_t sev, const char * message) {
	log_error_recovery_data.magic_number = LOG_ERROR_RECOVERY_MAGIC_NUMBER;
	log_error_recovery_data.error_level = sev;

	strncpy(log_error_recovery_data.error_msg_buffer, message, 254);
}
