/*
 * log_error_recovery.h
 *
 *  Created on: Sep 15, 2022
 *      Author: xaseiresh
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef ALL_LOG_LOG_ERROR_RECOVERY_H_
#define ALL_LOG_LOG_ERROR_RECOVERY_H_

#include "log.h"
#include "stdint.h"

#define NOINIT_ATTR __attribute__((section (".noinit")))

#define LOG_ERROR_RECOVERY_MAGIC_NUMBER 0x12345678

typedef struct {
	uint32_t magic_number;
	log_level_t error_level;
	char error_msg_buffer[255];
} error_recovery_section_t;

void log_error_recovery_init();

void log_error_mark_error(log_level_t severity, const char * message);

#endif /* ALL_LOG_LOG_ERROR_RECOVERY_H_ */
