/**
 * @author David Bailey (davidbailey.2889@gmail.com) 
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */


#include "log.h"

#include <stdint.h>

void log_print_stm32_uid() {
	const uint32_t * STM_ID_BITS = (const uint32_t*)0x1FFF7A10;

	LOGI("SYS", "STM32 Unique ID is: 0x%08X-%08X-%08X", STM_ID_BITS[0], STM_ID_BITS[1], STM_ID_BITS[2]);
}

void log_print_stm32_reset_reason() {
	const uint32_t RESET_SOURCE = *((const uint32_t*)(0x74+0x40023800));

	if((RESET_SOURCE & ((1<<30) || (1<<29))) != 0) {
		LOGE("SYS", "System reset reason: WATCHDOG");
	}
	else if((RESET_SOURCE & (1<<27)) != 0) {
		LOGI("SYS", "System reset reason: POWER-ON");
	}
	else if((RESET_SOURCE & (1<<25)) != 0) {
		LOGE("SYS", "System reset reason: BROWNOUT");
	}
	else if((RESET_SOURCE & (1<<26)) != 0) {
		LOGI("SYS", "System reset reason: NRST");
	}
	else if((RESET_SOURCE & (1<<28)) != 0) {
		LOGW("SYS", "System reset reason: SOFTWARE RST");
	}
	else {
		LOGW("SYS", "System reset reason: UNKNOWN");
	}
}
