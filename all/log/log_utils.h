/*
 * log_utils.h
 *
 *  Created on: 8 Sep 2022
 *      Author: xaseiresh
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef ALL_LOG_LOG_UTILS_H_
#define ALL_LOG_LOG_UTILS_H_


void log_print_stm32_uid();
void log_print_stm32_reset_reason();


#endif /* ALL_LOG_LOG_UTILS_H_ */
