/*
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
*/

#include "spi.h"

#include <all/log/log.h>

namespace SPI {

Master::Master() 
    : spi_lock_mutex_buffer(), spi_lock_mutex(nullptr),
      waiting_on_rx_task(nullptr), transfer_in_progress(false),
      rw_function(nullptr) {
}

void Master::init() {
	if(spi_lock_mutex) {
		LOGF("SPI", "Double initialization of SPI module, asserting error!");
		assert(false);
	}

    spi_lock_mutex = xSemaphoreCreateMutexStatic(&spi_lock_mutex_buffer);

    LOGI("SPI", "SPI Module %p initialized.", this);
}

void Master::set_rw_function(spi_rw_func_t function) {
    rw_function = function;
}

void Master::task_notify_rw_done() {
	transfer_in_progress = false;

    if(waiting_on_rx_task) {
    	BaseType_t task_woken = 0;

    	vTaskNotifyGiveFromISR(waiting_on_rx_task, &task_woken);

        if(task_woken)
        	portYIELD();
    }
}

bool Master::is_transfer_in_progress() {
	return transfer_in_progress;
}

Device::Device(Master & master) 
    : gpio_reg(nullptr), cs_pin(0),
	  master(master) {
}
Device::Device(Master & master, volatile uint32_t * gpio_reg, uint32_t cs_pin)
	: 	gpio_reg(gpio_reg), cs_pin(cs_pin),
		master(master) {

	deassert_cs();
}

void Device::assert_cs() {
    if(gpio_reg == nullptr)
        return;
    
    *gpio_reg &= ~(1<<cs_pin);
}
void Device::deassert_cs() {
    if(gpio_reg == nullptr)
        return;
    
    *gpio_reg |= (1<<cs_pin);
}


Transfer::Transfer(Device & device, bool claim) 
    : lock_count(0), device(device) {

    if(claim)
        this->claim();
}

Transfer::~Transfer() {
    // Ensure that for the destruction, ALL locks are released
    // and not just a single lock layer.
    if(lock_count > 0)
        lock_count = 1;

    release();
}

void Transfer::claim() {
    if(device.master.spi_lock_mutex == nullptr) {
        LOGF("SPI", "Transfer was attempted on master %x without Master.init() having been called!", &(device.master));
        vTaskDelay(20);
        assert(false);
    }

    if(lock_count > 0) {
        lock_count++;
        return;
    }

    xSemaphoreTake(device.master.spi_lock_mutex, portMAX_DELAY);
    lock_count++;

    device.assert_cs();
}

void Transfer::release() {
    if(lock_count > 1) {
        lock_count--;
        return;
    }

    if(lock_count == 1) {
        lock_count--;
        device.deassert_cs();
        xSemaphoreGive(device.master.spi_lock_mutex);
    }
}

void Transfer::rw(const void * tx, void * rx, size_t num_bytes) {
    if(device.master.rw_function == nullptr) {
        LOGF("SPI", "Transfer was attempted on master %x without Master.set_rw_function() having been set up.", &(device.master));
        vTaskDelay(20);
        assert(false);
    }
    
    claim();

    device.master.waiting_on_rx_task = xTaskGetCurrentTaskHandle();
    device.master.transfer_in_progress = true;

    device.master.rw_function(tx, rx, num_bytes);

    while(device.master.transfer_in_progress) {
    	ulTaskNotifyTake(1, 100);
    }

    device.master.waiting_on_rx_task = nullptr;

    release();

    // Sometimes this is necessary to reduce interference from other tasks
    xTaskNotifyStateClear(nullptr);
}

}
