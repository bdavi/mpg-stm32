/**
 * @file spi.h
 * @author David Bailey (david.bailey@ipp.mpg.de)
 * @brief Platform-Independent, FreeRTOS compatible wrapper for simple SPI peripherals
 * @version 0.1
 * @date 2022-05-18
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#pragma once

#include <FreeRTOS.h>
#include <semphr.h>

namespace SPI {

class Transfer;

/**
 * @brief SPI Read/Write function definition
 * @details This function is required by the Master class, and 
 *   is meant to handle the hardware specific Read/Write functionality.
 *   See the Master.rw_function documentation for more details
 * 
 * @sa Master.rw_function
 * 
 */
typedef void (*spi_rw_func_t)(const void * tx, void * rx, size_t num_bytes);

/**
 * @brief SPI Master class.
 * @details This class handles a single SPI Master instance, and acts
 *   as a wrapper to the physical hardware below. As such, only one
 *   of such instance should be created per SPI bus in use.
 * 
 *   The SPI Master includes the necessary mutexes to regulate access 
 *   between threads. It does *not* contain any buffers for SPI transfers,
 *   these are to be supplied by the user level code.
 *   The code is designed the blocking in all cases, and will return only
 *   once the SPI transfer is complete. Mutexes are used to ensure thread safety.
 * 
 *   CS line handling is left to the user code.
 *   The user MUST first claim the SPI bus by creating a new Device instance, 
 *   THEN assert their CS line. This ensures that other devices have completed their
 *   transfers.
 * 
 *   The user code must also implement the transfer of the SPI bus data, by 
 *   providing a function for read/write operations. This function MUST be able to 
 *   handle nullptr in tx and rx buffers, in case the user code only requires a read or
 *   write.
 *   Once the transfer is complete, the user code MUST call Master.task_notify_rw_done().
 *   This function will notify the task currently using the bus that the transfer has 
 *   completed. 
 * 
 *   Individual SPI devices are defined by the Device class, allowing the user to 
 *   specify a Chip-Select line for automated CS line assertion. Please note that 
 *   no additional locking on a device level is implemented, so if exclusive access 
 *   to a device is required, an additional Mutex must be used.
 * 
 *   Performing actual data transfers is done by the Transfer. It provides an easier 
 *   to use locking mechanism, as it implicitly locks the mutex in the constructor, and 
 *   unlocks it in the deconstructor. This eases the burden on the user, as they no longer 
 *   need to worry about locking/unlocking, and unlocking is guaranteed when the Transfer 
 *   is destroyed, i.e. at a `return` statement.
 * 
 *   An example to use the STM32 SPI HAL is as follows:
@code

Master test_spi_tx;

// This function needs to be passed to the hspi completed callbacks, as given
// below
void spi1_done_fn(SPI_HandleTypeDef * spi) {
	test_spi_tx.task_notify_rw_done();
}

// This function handles the SPI Read/Write ops, and is passed to Master.set_rw_function
void spi1_send_rw(const void *tx, void *rx, size_t num) {
	uint8_t * tx_ptr = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(tx));
	uint8_t * rx_ptr = reinterpret_cast<uint8_t *>(rx);

	if(tx == nullptr && rx == nullptr) {
		test_spi_tx.task_notify_rw_done();
	}
	else if(tx == nullptr) {
		HAL_SPI_Receive_DMA(&hspi1, rx_ptr, num);
	}
	else if(rx == nullptr) {
		HAL_SPI_Transmit_DMA(&hspi1, tx_ptr, num);
	}
	else {
		HAL_SPI_TransmitReceive_DMA(&hspi1, tx_ptr, rx_ptr, num);
	}
}

// Example init function
void init_spi_callbacks() {
    hspi1.TxRxCpltCallback = spi1_done_fn;
	hspi1.TxCpltCallback = spi1_done_fn;
	hspi1.RxCpltCallback = spi1_done_fn;

    test_spi_tx.init();
	test_spi_tx.set_rw_function(spi1_send_rw);
}

@endcode
 */
class Master {
protected:
friend Transfer; //!< Allow friend access to the Transfer

    //! Static buffer for the FreeRTOS SPI locking mutex
    StaticSemaphore_t spi_lock_mutex_buffer;
    //! Handle to the SPI locking mutex
    xSemaphoreHandle  spi_lock_mutex;

    //! TaskHandle pointing to the task currently waiting on a SPI transfer to finish
    TaskHandle_t waiting_on_rx_task;
    //! Flag to mark that a transfer is actively ongoing.
    volatile bool transfer_in_progress;

    /**
     * @brief SPI Read/Write function
     * @details This is a pointer to a function that shall be provided by the user.
     *   The function must initialize the SPI transfer, and may block. If it does
     *   block, however, it should do so in a way that frees up the FreeRTOS 
     *   scheduler. The STM32 HAL functions may be blocking with no FreeRTOS yield, and 
     *   may be unsuitable.
     * 
     *   Preferably, this function will start a transfer using DMA/ISR and return immediately.
     *   The user must then use a transfer complete callback, and call task_notify_rw_done() once
     *   the SPI transfer is fully completed.
     * 
     * @sa set_rw_function
     * @sa task_notify_rw_done
     */
    spi_rw_func_t rw_function;
    
public:
    /**
     * @brief Construct a new Master object
     * @details There should be one SPI Master per physical SPI peripheral in
     *   use by this system. After constructing the SPI Master, 
     *   set_rw_function() MUST be used to provide an implementation of a RW 
     *   SPI code.
     * 
     * @sa rw_function
     * @sa set_rw_function
     */
    Master();
    
    /**
     * @brief Initialize the SPI Master
     * @details This will set up the
     *   internal mutex needed to negotiate SPI access, as 
     *   well as a few other internal settings.
     *   MUST be called before being able to use the SPI bus
     *   via Transfer. Instantiation of SPIDevices is still
     *   safe
     * 
     */
    void init();

    /**
     * @brief Set the SPI Read/Write function
     * @sa rw_function
     * 
     * @param function New SPI Read/Write function to use 
     */
    void set_rw_function(spi_rw_func_t function);

    /**
     * @brief Notify a waiting task about SPI transfer completion
     * @details This function MUST be called by the SPI Read/Write function
     *   after it completed the SPI transfer. It can be called from an interrupt.
     *   It will send a task notification to the waiting task and mark the transfer
     *   as complete.
     * 
     */
    void task_notify_rw_done();

    bool is_transfer_in_progress();
};

/**
 * @brief Device class to define a single device
 * @details This class is a simple container to define how to access a single device.
 *   More precisely, it allows the user to define a chip-select line in combination 
 *   to the SPI Master device, which is used in the Transfer class for 
 *   automatic CS assertion/release.
 * 
 *   After using one of the Device() constructors, the code can then instantiate a 
 *   Transfer to perform an actual transfer. Please note that SPIDevices are 
 *   intended to be persistent, whereas a Transfer is intended to be deconstructed
 *   once the SPI data has been transferred.
 */
class Device {
protected:
friend Transfer; //!< Allows friend access to the SPI transfer

    //! Optional pointer to the register that contains the Chip-Select bit.
    //! (1<<cs_pin) will be cleared from gpio_ptr after claiming, and will be 
    //! set in gpio_ptr before relase. 
    //! Can be nullptr, in which case it is ignored
    volatile uint32_t * const gpio_reg;
    //! Optional pin number to use, see gpio_ptr
    const uint32_t cs_pin;

public:
    //! Reference to the Master used to handle communications
    Master & master;

    //! Allow the user to pull the CS line low
    void assert_cs();
    //! Allow the user to pull the CS line high
    void deassert_cs();
    
    /**
     * @brief Construct a new Device object without chip-select
     * 
     * @warning When using this version of instantiation, no
     *   further handling of the chipselect line is provided by this
     *   function. User code should handle CS lines itself, and 
     *   it MUST ensure that the CS line is released whenever the 
     *   SPI device has released the SPI line!
     *   It is recommended to use Device(Master &, volatile uint32_t *, uint32_t)
     * @param master Reference to the SPI bus to be used by this device
     */
    Device(Master & master);

    /**
     * @brief Construct a new Device object
     * @details This will construct a new Device class, with a pointer to 
     *   the GPIO register for CS line handling. This means that the Device
     *   will handle claiming/releasing the SPI lock and CS line of this device.
     * @note This will not set up the GPIO pin for output, this should be done 
     *   either by user- or HAL-Code.
     * 
     * @param master SPI Master instance to use
     * @param gpio_reg Pointer to the register of the GPIO Pin (GPIOx->ODR)
     * @param cs_pin   Number of the CS pin to use.
     */
    Device(Master & master, volatile uint32_t * gpio_reg, uint32_t cs_pin);
};

/**
 * @brief SPI Device class
 * @details This is a wrapper class around the functionality
 *   that Master provides. It is meant as a way to ease 
 *   locking/unlocking of the SPI communication mutex, and
 *   to provide a simple way to handle master read/writes.
 * 
 *   This class should only be instantiated within a temporary 
 *   context, i.e. part of a function that requires the SPI transfer.
 *   It shall preferably only be used for single transfers, but may
 *   be re-used indefinitely if the user handles locking/unlocking
 *   of the SPI bus with the claim() and release() functions.
 * 
 *   An example use would be:
 * @code
// Static definition of a certain SPI device here
Device device(spi_line_1, &(GPIOD->ODR), DEVICE_CS_Pin);

void someSPIFunction() {
    // Instantiating a SPI transfer here. This 
    // will instantly claim the SPI bus mutex, then
    // assert the CS line.
    Transfer spi(device); 

    const char * hello = "Hello world!";

    // Perform a transfer with arbitrary data and length.
    // Will block until the SPI transfer has fully completed.
    spi.rw(hello, nullptr, strlen(hello));

    // Optionally release the mutex lock temporarily
    // This will also de-assert the CS line, if passed
    spi.release();
    
    vTaskDelay(100);

    // Will automatically claim the SPI bus, re-assert the CS line
    // for transfer, and de-assert afterwards. Use claim() to 
    // permanently acquire a SPI bus lock and keep the CS line asserted
    // between multiple rw operations
    char data[128] = {};
    spi.rw(data, data, 128);

    // In the deconstructor, the Device will always release 
    // the SPI bus. This avoids unintended permanent locks on the SPI bus
    // and eases usage.
}
 * @endcode 
 *
 * @note This class is not thread-safe! The user should use one Transfer per 
 *   thread, and preferably one Transfer per function. This is supposed 
 *   to be a short-lived, local-only class, to make use of the 
 *   auto-release on deconstruct feature.
 */
class Transfer {
private:
    // Internal variable, counts the number of claims/releases to implement
    // a cheap recursive mutex
    int lock_count;

public:
    //! Reference to the SPI Device to use for data transfers
    Device & device;

    /**
     * @brief Prepare a new SPI Transfer
     * @details This will initialize a new Transfer object.
     *   If the claim parameter was left set to true, it will then 
     *   claim the SPI Line, which will take the SPI mutex then assert
     *   the CS line. The user may immediately call Transfer.rw() to 
     *   send and receive data.
     *   Upon deconstructing the SPI lock and CS line will be released. 
     *   Care should be taken if the Transfer is instantiated in a static context, i.e.
     *   the instance is persistent. In this case, the claim parameter should be false,
     *   and claim() and release() should be used as required.
     * 
     * @note This class is meant to be short-lived, and should be instantiated only 
     *   in local contexts where needed.
     * @note This class is not thread-safe, and should only be used within one single 
     *   thread. The SPI system itself IS thread-safe through the use of a mutex and 
     *   multiple SPI Transfer classes per thread.
     * @warning If the Device was not initialized with chip-select lines in place, 
     *   no automatic CS line handling will be performed. The user-code must ensure 
     *   that the CS line is unlocked before the SPI Transfer unlocks the SPI mutex!!
     * 
     * @param device 
     * @param claim 
     */
    Transfer(Device & device, bool claim = true);
    ~Transfer();

    /**
     * @brief Claim SPI bus and assert CS line
     * @details This function will block until the lock of the SPI bus was acquired.
     *  It will then assert the CS line.
     * 
     *  This function is recursion-safe, and can be called multiple times.
     *  The Transfer will also de-assert the CS line and SPI bus when 
     *  deconstructing.
     */
    void claim();
    /**
     * @brief Deassert the CS line and release the SPI bus lock.
     */
    void release();

    /**
     * @brief Perform a SPI read-write transfer
     * @details This function will perform a SPI Read/Write transfer. 
     *   It will first claim the SPI bus, acquiring the lock and asserting the
     *   CS line (unless this was already done by a previous claim()).
     *   It will then block until the SPI data transfer is complete, and return.
     *   It will then release CS line and SPI lock UNLESS a previous claim() was used, 
     *   in which case lock and CS remain asserted.
     * 
     *   This allows both for easy one-off transfers as well as more 
     *   complex multi-part sequences.
     * 
     * @param tx Pointer to the data to be sent out. Can not be nullptr!
     *   In case only receiving is of interest, the user may point tx to the
     *   rx buffer, and initialize the buffer to zeroes (meaning only '0' will
     *   be pushed onto the SPI bus).
     * @param rx Optional pointer to the receive buffer. May be nullptr if received
     *   data is not of interest.
     * @param num_bytes Number of bytes to R/W over SPI
     */
    void rw(const void * tx, void * rx, size_t num_bytes);
};

}
