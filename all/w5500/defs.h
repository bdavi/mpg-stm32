/**
 * @file all/w5500/defs.h
 * @author David Bailey (david.bailey@ipp.mpg.de)
 * @brief W5500 Structure definitions
 * @version 0.1
 * @date 2022-05-30
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#pragma once

#include <stdint.h>

namespace W5500 {
/**
 * @brief W5500 Register definitions
 * @details This namespace defines the naming, location/offsets and structure
 *   of the internal registers of the W5500 chip, for those systems that are used here.
 *   Thusly, the rest of the W5500 code relies heavily on these register definitions
 */
namespace REGS {

//! Enum defining the register offsets for the Common Register Block of the W5500
enum crb_offsets : uint16_t {
    CRB_MODE            = 0,
    CRB_GATEWAY_ADDR    = 0x1,
    CRB_SUBNET_MASK     = 0x5,
    CRB_SOURCE_MAC      = 0x9,
    CRB_SOURCE_IP       = 0xF,
    CRB_INTERRUPT_LEVEL_TIMER = 0x13,   // Interrupt assert delay of (INTLEVEL + 1)*PLLclk*4
    CRB_ISR             = 0x15,
    CRB_ISR_MASK        = 0x16,
    CRB_SOCK_ISR        = 0x17,
    CRB_SOCK_ISR_MASK   = 0x18,
    CRB_RETRY_TIME      = 0x19,         // Default value of 200ms is OK. (100us * RETRY)
    CRB_RETRY_COUNT     = 0x1B,
    CRB_UNREACHABLE_IP  = 0x2C,
    CRB_PHY_CONFIG      = 0x2E,
	CRB_VERSIONR		= 0x39
};

//! Enum defining the register offsets of the Socket Configuration register block
enum sock_offsets : uint16_t {
    SOCK_MODE       = 0,
    SOCK_COMMAND    = 0x1,
    SOCK_ISR        = 0x2,
    SOCK_STATUS     = 0x3,
    SOCK_SOURCE_PORT = 0x4,
    SOCK_DEST_ADDR  = 0x6,
    SOCK_DEST_IP    = 0xC,
    SOCK_DEST_PORT  = 0x10,
    SOCK_SEGMENT_TIME = 0x12,
    SOCK_IP_TOS     = 0x15,
    SOCK_IP_TTL     = 0x16,
    SOCK_RX_BFR_SIZE = 0x1E,
    SOCK_TX_BFR_SIZE = 0x1F,
    SOCK_TX_FREE_SIZE   = 0x20,
    SOCK_TX_READ_PTR    = 0x22,
    SOCK_TX_WRITE_PTR   = 0x24,
    SOCK_RX_SIZE        = 0x26,
    SOCK_RX_READ_PTR    = 0x28,
    SOCK_RX_WRITE_PTR   = 0x2A,
    SOCK_ISR_MSK        = 0x2C,
    SOCK_FRAG_OFFSET    = 0x2D,
    SOCK_KEEPALIVE      = 0x2F,     // Unit: 5 seconds
};

////
// ALL BELOW STRUCTS MUST BE TIGHT-PACKED
// They are used during communication with the SPI chip!
// Also notw: Any 16-bit number is BIG-ENDIAN (network endianness), and might 
// require a byte-swap
#pragma pack(1)

//! CRB MODE Register (0x0) definition
struct crb_mode_t {
    uint8_t :1;         //!< Paddinng
    uint8_t farp:1;     //!< Force ARP request for each data send if set 1
    uint8_t :1;         //!< Padding
    uint8_t pppoe:1;    //!< PPPoE Mode, used for ADSL, unimplemeneted here
    uint8_t ping_block:1; //!< Ping block mode, set to 1 to block pings, unimplemented
    uint8_t wol_enable:1; //!< Enable Wake On Lan.
    uint8_t :1;
    uint8_t sw_reset:1;   //!< Write to 1 to initiate a software reset
};

//! CRB ISR (0x15)/ISR_MASK (0x16) register definition
struct crb_isr_t {
    uint8_t _reserved:4;    //!< Reserved
    uint8_t magic_packet:1; //!< Magic Packet Interrupt (for Wake On Lan)
    uint8_t pppoe_close:1;  //!< PPPoE Signal (unimplemented)
        /** Flagged when destination IP was unreachable. 
         * CRB_UNREACHABLE_IP is then set.*/
    uint8_t dest_unreachable:1; 
    uint8_t ip_conflict:1; //!< An IP conflict was detected
};

//! CRB PHY CONFIG (0x2E) register definition 
struct crb_phy_config_t {
    uint8_t link_status:1;      //!< 1 if Phy link exists. Read-Only
    uint8_t speed_status:1;     //!< 0 for 10MBit, 1 for 100MBit. Read-Only
    uint8_t duplex_status:1;    //!< 0 for half-duplex, 1 for full-duplex
    uint8_t opmode:3;           //!< Opmode will be either AUTO (0b111) or forced 100MBit/s full duplex (0b011)
    uint8_t opmode_configure:1; //!< Set this bit to 1 to use opmode for link control
    uint8_t reset:1;            //!< Reset phy by writing a 0. User must write 1 again
};

/**
 * @brief CRB address definition shorthand.
 * @details This struct defines the four parameters that need
 *   to be set in the CRB to allow for proper internet communication, these
 *   being gateway, subnet, source MAC and IP.
 * 
 *   As these values reside close to one another in memory, it makes sense 
 *   to write them all in one go.
 */
struct crb_address_config_t {
    uint8_t gateway[4];             //!< Gateway IPv4
    uint8_t subnet_mask[4];         //!< Subnet mask (in IPv4 format)
    uint8_t source_hw_address[6];   //!< Source MAC address (as uint8_t array)
    uint8_t source_ip[4];           //!< Source IPv4 address
};

/**
 * @brief Socket mode register
 * @details This register defines the behavior of the socket when the SOCK_CR_OPEN
 *   command is given. It sets TCP/UDP mode and a few other small config parameters.
 */
struct sock_mode_register_t {
    uint8_t mode:4; //!< Socket Mode. 0->Closed, 1->TCP, 2->UDP, 4->RAW (unimplemented)
    uint8_t unicast_block:1; //!< 1 enables Unicast Blocking
    uint8_t nodelay_multicast:1; //!< For TCP: Use No-delay ACK (faster TX), for UDP: 0->IGMPv2, 1->IGMPv1
    uint8_t block_udp_broadcast:1; //!< 1->Blocks receive of broadcasting
    uint8_t multicast_enable:1; //!< Only for UDP: 1->Enable Multicast. Socket IP and Socket PORT specify multicast group IP and port
};

/**
 * @brief Socket command register commands
 * @details This enum specifies the types of commands to be fed into the
 *   SOCK_COMMAND register. They handle opening, connecting, and closing.
 * 
 *   The usual flow for them is:
 *     Write target IP/Port/other config -> OPEN -> LISTEN/CONNECT (for TCP)
 *     SEND/RECV to send packets -> CLOSE
 *   Upon a socket error that leaves the socket closed, calling OPEN suffices
 *   to reset the port
 */
enum sock_command_type : uint8_t {
    SOCK_CR_OPEN    = 0x1,  //!< Ready the port for read/writing
    SOCK_CR_LISTEN  = 0x2,  //!< Opens a TCP server at SOURCE_PORT. DEST_IP and DEST_PORT are set when a client connects
    SOCK_CR_CONNECT = 0x4,  //!< Connects to a TCP server at DEST_IP and DEST_PORT
    SOCK_CR_DISCON  = 0x8,  //!< Disconnect a open connection (TCP server or client)
    SOCK_CR_CLOSE   = 0x10, //!< Close a socket
    SOCK_CR_SEND    = 0x20, //!< SEND data. Will send all newly written data in the TX buffer.
    SOCK_CR_SEND_MAC  = 0x21, //!< IN UDP MODE, send data to socket DHAR (avoids ARP, faster)
    SOCK_CR_SEND_KEEP = 0x22, //!< IN TCP MODE, send a keepalive ping (ignored when auto keepalive is enabled)
    SOCK_CR_RECV      = 0x40  //!< Finish receiving of data
};

//! Socket interrupt flags
struct sock_isr_t {
    uint8_t con:1;      //!< Set in TCP client/server mode after a cnnection was established
    uint8_t discon:1;   //!< Set in TCP client/server mode after a connection was disconnected (expected or not)
    uint8_t recv:1;     //!< Set when a data packet was received (TODO verify!)
    uint8_t timeout:1;  //!< Set when a keepalive packet (+retry) was missed. Use to reconnect
    uint8_t send_ok:1;  //!< Set when all data in the buffer was sent out properly
};

//! SOCK_STATUS type definitions
enum sock_status_type : uint8_t {
    CLOSED = 0,                 //!< This port is curretnly not used
    SOCK_INIT   = 0x13,         //!< After SOCK_CR_OPEN, the port will be in INIT. UDP can send, TCP must CONNECT/LISTEN
    SOCK_LISTEN = 0x14,         //!< TCP Server has been opened and is waiting for a client to connect
    SOCK_ESTABLISHED = 0x17,    //!< TCP Connection has been established
    SOCK_CLOSE_WAIT  = 0x1C,    //!< The socket is waiting to fully close
    SOCK_UDP    = 0x22,         //!< The socket is opened in UDP Mode and can be used to send/receive data
    SOCK_MACRAW = 0x42          //!< Socket 0 is opened in MACRAW mode and can be used directly as ETH PHY
};

//! Convenience struct to configure the whole socket address/port registers in one go
struct sock_conn_config_t {
    uint16_t source_port;   //!< Source port to send from (WARNING: NETWORK/BIG ENDIANNESS)
    uint8_t  dest_mac[6];   //!< Destination MAC (only for UDP SEND_MAC command, bypasses ARP)
    uint8_t  dest_ip[4];    //!< Destination IP to send to (UDP) or connect to (TCP)
    uint16_t dest_port;     //!< Destination port to send/connect to. NETWORK/BIG ENDIAN
};

/**
 * @brief SPI header definition struct
 * @details This struct must be sent at the start of each new SPI frame. It sets 
 *   whether or not we are reading or writng data to the registers, and selects which
 *   register to use.
 * 
 *   Note: In this implementation the communication mode is hardcoded to use the CS line
 *   (variable data length mode).
 */
struct spi_header_t {
    uint16_t addr_offset;   //!< Offset to write into. CAUTION: BIG ENDIAN! Swap bytes first!
    uint8_t opmode:2;       //!< Transfer operation mode. SET TO 0 FOR VARIABLE LENGTH MODE (CS LINE), other modes unimplemented
    uint8_t rw:1;           //!< Read (0) or write(1) data to the register
    uint8_t regtype:2;      //!< Which register type to write into (0->CRB, 1->Socket mode, 2->Socket TX, 3->Socket RX)
    uint8_t socket_no:3;    //!< Which socket to RW from (0 for CRB!)
};

#pragma pack(0)

}
}
