/**
 * @file all/w5500/w5500.h
 * @author  David Bailey (david.bailey@ipp.mpg.de)
 * @brief 
 * @version 0.1
 * @date 2022-06-02
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#pragma once

#define W5500_EXP_IP(ip) ip[0], ip[1], ip[2], ip[3]

#define W5500_ASSIGN_IP(array, ia, ib, ic, id) do {\
	array[0] = ia;\
	array[1] = ib;\
	array[2] = ic;\
	array[3] = id;\
} while(false)

/**
 * @brief W5500-Ethernet PHY namespace
 * @details This namespace contains all necessary modules to use the 
 *   W5500 Ethernet chip, to initialize it, establish connections with 
 *   TCP and UDP servers, and to maintain and control data transfer through it.
 * 
 *   Currently, the following is suported:
 *   - IPv4, fixed address
 *   - TCP Server and Client
 *   - UDP connections
 *   - Logging of important events built-in
 * 
 *   MACRAW is *not* supported. As such, communication is limited to eight used
 *   ports simultaneously. 
 *   
 *   To be implemented:
 *   - DHCP
 *   - DNS
 *   - SNTP
 *   - MQTT
 * 
 *   The code is split as follows:
 *   - Register definitions in the defs.h file (REGS namespace)
 *   - Core PHY controller, handles initializing and communicating with the W5500 
 *      chip, handing out socket numbers, distributing interrupts, etc.
 *   - Socket wrapper class (BaseSocket). This class represents a socket 
 *     configuration. It handles (auto re-)connecting, and provides 
 *     blocking read/write FreeRTOS compliant functions to interact with
 *     the socket connection.
 * 
 *   In a classical setup, the user will first define the SPI communication channel
 *   for the W5500, using the SPI::Master class. They must then first initialize a Core, 
 *   spawn a new FreeRTOS Thread with sufficient stack (*Note:* All read-interrupts may
 *   run on the Ethernet phy RW thread, and may require larger stack!), call Core.init(), 
 *   then start a infinite loop calling Core.check_isr()
 * 
 *   It is recommended to connect the W5500's ISR line to a GPIO EXTI line, and call Core.isr_flagged()
 *   to decrease latency.
 * 
 *   From there, other threads may spawn BaseSocket instances, open communication via 
 *   start(const socket_config &) and utilize lock_buffer(), load_buffer(), send() and send_buffer()
 *   to send data packets. These return quickly, but may block to wait for concurrent buffer access.
 *   Reading should best be handled via a callback routine, but the user may use blocking 
 *   functions to wait for data.
 * 
 * @todo Some functions still need implementation, as well as higher level protocols.
 *   Most importantly will be DHCP, then DNS and SNTP. Later on MQTT at QOS 0 only 
 *   would be good as well.
 * 
 */
namespace W5500 {}

#include "w5500_core.h"
#include "w5500_socket.h"
