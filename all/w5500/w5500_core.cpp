/**
 * @file w5500_core.cpp
 * @author David Bailey (davidbailey.2889@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2022-05-31
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */


#include "w5500.h"

#include <all/log/log.h>
#include <cstring>

namespace W5500 {

using namespace REGS;
using namespace SPI;

Core::Core(Device device) :
	sockets(), sock_isr_mask(0),
    last_socket_handle_tick(0),
	w5500_thread_handle(nullptr),
	isr_notify_flag(false),
	chip_initialized(false),
	phy_present(false),
	chip_mac(),
	spi(device) {

    socket_access_mutex = xSemaphoreCreateMutexStatic(&socket_access_mutex_buffer);
}

void Core::run_handler_tick() {
    if((xTaskGetTickCount() - last_socket_handle_tick) >= (1000/portTICK_PERIOD_MS)) {
        
        crb_phy_config_t phy_cfg = {};
        read_crb(CRB_PHY_CONFIG, &phy_cfg, 1);

        if(phy_cfg.link_status != phy_present) {
        	if(phy_cfg.link_status)
        		LOGI("W5500", "Phy connection reestablished");
        	else {
                for(int i=0; i<8; i++) {
                    if(sockets[i]) {
                        sockets[i]->phy_disconnected();
                    }
                }
        		LOGW("W5500", "Phy connection lost!");
            }

        	phy_present = phy_cfg.link_status;
        }

        // Advance the timer forwards by a second to give better regularity
        last_socket_handle_tick += 1000/portTICK_PERIOD_MS;

        // Ensure that if the handler call was delayed, it doesn't aggressively run
        // ten times in a row before having caught up
        if(((xTaskGetTickCount() - last_socket_handle_tick)) > 5000) {
            last_socket_handle_tick = xTaskGetTickCount();
        }

        for(int i=0; i<8; i++) {
            if(sockets[i]) {
                sockets[i]->handler_tick();
            }
        }
    }
}

void Core::run_check_isr() {
	uint8_t interrupts = 0;
	read_crb(CRB_SOCK_ISR, &interrupts, 1);
	write_crb(CRB_SOCK_ISR, &interrupts, 1);

	if(interrupts == 0) {
        return;
    }

    for(int i=0; i<8; i++) {
		if((interrupts & (1<<i)) != 0) {
			if(sockets[i]) {
				sockets[i]->check_isr();
			}
			else {
				LOGD("W5500", "Socket ISR for unused socket %d flagged?", i);
			}
		}
	}
}

void Core::read(uint8_t regtype, uint8_t sock_no, uint16_t offset, void * rx_buffer, size_t num) {
    spi_header_t spi_header = {};
    spi_header.addr_offset = ((offset & 0xFF) << 8) | ((offset & 0xFF00) >> 8);
    spi_header.opmode = 0;
    spi_header.rw = 0;
    spi_header.regtype = regtype;
    spi_header.socket_no = sock_no;

    Transfer transfer(spi);

    transfer.rw(&spi_header, nullptr, sizeof(spi_header));
    transfer.rw(nullptr, rx_buffer, num);
}

void Core::write(uint8_t regtype, uint8_t sock_no, uint16_t offset, const void * tx_buffer, size_t num) {
    spi_header_t spi_header = {};
    spi_header.addr_offset = ((offset & 0xFF) << 8) | ((offset & 0xFF00) >> 8);
    spi_header.opmode = 0;
    spi_header.rw = 1;
    spi_header.regtype = regtype;
    spi_header.socket_no = sock_no;

    Transfer transfer(spi);

    transfer.rw(&spi_header, nullptr, sizeof(spi_header));
    transfer.rw(tx_buffer, nullptr, num);
}

void Core::read_crb(crb_offsets reg, void * rx_buffer, size_t num) {
    read(0, 0, reg, rx_buffer, num);
}
void Core::write_crb(crb_offsets reg, const void * tx_buffer, size_t num) {
    write(0, 0, reg, tx_buffer, num);
}

int Core::claim_socket(BaseSocket & sock_ptr) {
    xSemaphoreTake(socket_access_mutex, portMAX_DELAY);

	for(int i=0; i < 8; i++) {
		if(sockets[i] == nullptr) {
			sockets[i] = &sock_ptr;

			sock_isr_mask |= 1<<i;
			write_crb(CRB_SOCK_ISR_MASK, &sock_isr_mask, 1);

            xSemaphoreGive(socket_access_mutex);
			return i;
		}
	}
    
    xSemaphoreGive(socket_access_mutex);
	return -1;
}
void Core::release_socket(int i) {
    xSemaphoreTake(socket_access_mutex, portMAX_DELAY);

    sockets[i] = nullptr;
	sock_isr_mask &= ~(1<<i);
	write_crb(CRB_SOCK_ISR_MASK, &sock_isr_mask, 1);

    xSemaphoreGive(socket_access_mutex);
}

void Core::init() {
    LOGI("W5500", "Initializing ETH Chip %p", this);

    crb_mode_t crb_mode = {};

    // Perform software reset 
    crb_mode.sw_reset = 1;
    write_crb(CRB_MODE, &crb_mode, 1);
    
    vTaskDelay(10);

    uint8_t dummy_u8;
    read_crb(CRB_VERSIONR, &dummy_u8, 1);

    if(dummy_u8 != 0x04) {
    	LOGE("W5500", "Version nr. did not read back as 0x04 (found: %x).", dummy_u8);
    	LOGE("W5500", "Ethernet chip either not available or wrong version. Discontinuing.");

    	return;
    }

    crb_mode.sw_reset = 0;
    crb_mode.wol_enable = 1;
    write_crb(CRB_MODE, &crb_mode, 1);

    dummy_u8 = 3;
    write_crb(CRB_RETRY_COUNT, &dummy_u8, 1);

    crb_phy_config_t phy_cfg = {};
    phy_cfg.opmode = 0b011;
    phy_cfg.opmode_configure = 1;

    write_crb(CRB_PHY_CONFIG, &phy_cfg, 1);

    phy_cfg.reset = 1;
    write_crb(CRB_PHY_CONFIG, &phy_cfg, 1);

    chip_initialized = true;

    // Reset sockets, these can sometimes hang
    for(int i=0; i<8; i++) {
		uint8_t close_cmd = REGS::SOCK_CR_CLOSE;
		uint8_t zero = 0;
		write(1, i, REGS::SOCK_COMMAND, &close_cmd, 1);
		write(1, i, REGS::SOCK_ISR_MSK, &zero, 1);
    }

    int msg_counter = 20;
    int msg_backoff = 0;
    static const int notify_backoff[5] = { 1000, 3000, 5000, 10000, 30000 };

    TickType_t wait_start_tick = xTaskGetTickCount();

    while(!phy_cfg.link_status) {
        read_crb(CRB_PHY_CONFIG, &phy_cfg, 1);
        vTaskDelay(50/portTICK_PERIOD_MS);

        if(--msg_counter == 0) {
        	msg_counter = msg_backoff < 5 ? notify_backoff[msg_backoff] : 60000;
        	msg_backoff++;
        	msg_counter /= 50;

        	LOGW("W5500", "Waiting on phy link (for %dms)", xTaskGetTickCount() - wait_start_tick);
        }
    }

    phy_present = true;
    LOGI("W5500", "W5500 initialization complete.");
}

void Core::enable_int() {
	uint8_t tmp = 10;
	write_crb(CRB_INTERRUPT_LEVEL_TIMER, &tmp, 1);

	tmp = 0;
	write_crb(CRB_ISR_MASK, &tmp, 1);
}

void Core::notify_from_isr() {
    if(w5500_thread_handle) {
        // FreeRTOS notify from ISR does not immediately yield, so we need to manually boop the portYIELD

    	isr_notify_flag = true;

        BaseType_t thread_woken = 0;
        xTaskNotifyFromISR(w5500_thread_handle, 0, eNoAction, &thread_woken);
        portYIELD_FROM_ISR(thread_woken);
    }
}

void Core::loop_isr() {
	if(!chip_initialized) {
		LOGE("W5500", "Attempted running the ISR check on an uninitialized chip, freezing thread.");
		while(1) { vTaskDelay(portMAX_DELAY); }
	}

	w5500_thread_handle = xTaskGetCurrentTaskHandle();

    LOGI("W5500", "Entering Ethernet handling loop.");

    while(true) {
    	// Inner loop is meant to wait for an event.
    	// Event sources are either an ISR notification sent from the EXTI interrupt line (isr_notify_flag)
    	// or otherwise the 1s handling tick for the different sockets
    	while(true) {
    		if((xTaskGetTickCount() - last_socket_handle_tick) >= (1000/portTICK_PERIOD_MS))
    			break;
    		if(isr_notify_flag)
    			break;

    		TickType_t wait_time = std::min<uint32_t>(1000/portTICK_PERIOD_MS,
					1000/portTICK_PERIOD_MS - (xTaskGetTickCount() - last_socket_handle_tick));

			// Wait at most 1000ms for a ping to prevent overflow wait issues,
			// and not more than for the next socket "handle_tick"
			xTaskNotifyWait(0, 0, nullptr, wait_time);
    	}

    	isr_notify_flag = false;

        run_check_isr();
        run_handler_tick();
    }
}

void Core::configure_address(const REGS::crb_address_config_t & config) {
    if(!chip_initialized) {
    	LOGE("W5500", "Attempted setting address on an uninitialized chip, aborting");
    	return;
    }

	write_crb(CRB_GATEWAY_ADDR, &config, sizeof(config));

	LOGI("W5500", "MAC is %02x:%02x:%02x:%02x:%02x:%02x",
    		config.source_hw_address[0], config.source_hw_address[1], config.source_hw_address[2],
			config.source_hw_address[3], config.source_hw_address[4], config.source_hw_address[5]);

    LOGI("W5500", "IP Address configured to: %d.%d.%d.%d (Mask %d.%d.%d.%d)", 
        config.source_ip[0], config.source_ip[1], config.source_ip[2], config.source_ip[3],
        config.subnet_mask[0], config.subnet_mask[1], config.subnet_mask[2], config.subnet_mask[3]);
        
    LOGI("W5500", "Gateway address is:  %d.%d.%d.%d",
    	config.gateway[0], config.gateway[1], config.gateway[2], config.gateway[3]);
}

bool Core::is_initialied() {
	return chip_initialized;
}
bool Core::has_link() {
	return phy_present;
}

}
