/**
 * @file all/w5500/w5500_core.h
 * @author David Bailey (david.bailey@ipp.mpg.de)
 * @brief Header file of the core element of the W5500 Ethernet chip
 * @version 0.1
 * @date 2022-05-30
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#pragma once

#include <all/spi/spi.h>

#include "defs.h"

#include <array>

namespace W5500 {

class BaseSocket;

/**
 * @brief Core W5500 Handler class
 * @details This class represents the W5500 global configuration and communication 
 *   interface. 
 *   
 *  To the user, it provides options to initialize the W5500 and manage the interrupt flags, 
 *  and provides methods to set the IPv4 configuration. It also automatically handles 
 *  distributing the W5500's sockets for the BaseSocket class.
 * 
 *  Its first task is to implement abstracted read-write communication methods to interface 
 *  with the chip over SPI, utilizing a SPI::Device to provide FreeRTOS-Threadsafe concurrent
 *  access to the bus.
 *  Its second task is to initialize the chip, set important configuration flags as well 
 *  as the initial global IP configuration.
 *  Its third task is to then loop and read the ISR Flags of the W5500 chip, handing out 
 *  interrupts to their respective BaseSocket classes. 
 * 
 *  Configuration of the actual connections, handling auto-reconnections, etc., is 
 *  all done inside the BaseSocket implementation.
 * 
 *   An example use case may look as follows:
 * @code
W5500::Core w5500_core;

void w5500_thread(void *args) {
    w5500_core.init();


    // The below example is for a static IP configuration:

    W5500::crb_address_config_t ip_config = {};
    W5500_ASSIGN_IP(ip_config.source_ip, 192, 168, 178, 200);
    W5500_ASSIGN_IP(ip_config.gateway, 192, 168, 178, 1);
    W5500_ASSIGN_IP(ip_config.subnet_mask, 255, 255, 255, 0);

    w5500_core.configure_address(ip_config);

    // Now the device has an estabished connection, the user may perform additional 
    // configurations for their own sockets to set them up, send initial 
    // messages, etc.
    // Additionally, any DNS fetching should be done here.

    // Once done, call the ISR loop function.
    // This will block completely and continually read 
    // sockets, handle reconnects, etc.
    w5500_core.loop_isr();
}
 * @endcode
 * 
 * @note The user MUST provide a FreeRTOS thread calling the Core.loop_isr() function,
 *   after calling the Core.init() function.
 * 
 */

class Core {
private:
	StaticSemaphore_t socket_access_mutex_buffer;
    xSemaphoreHandle  socket_access_mutex;

    std::array<BaseSocket *, 8> sockets;
    uint8_t sock_isr_mask;

    TickType_t last_socket_handle_tick;

    TaskHandle_t w5500_thread_handle;
    volatile bool isr_notify_flag;


    void run_handler_tick();
    void run_check_isr();

protected:
friend BaseSocket;
    //! Indicates whether the W5500 is properly set up already or not. Socket config may not occur before this!
	bool chip_initialized;

	//! Indicates whether or not the W5500 has a PHY connection
	bool phy_present;

	uint8_t chip_mac[6];

    //! Internally used SPI device, handles threadsafe SPI access to a common bus
    SPI::Device spi;

    /**
     * @brief Read from a given register.
     * 
     * @param regtype       Register type. 0 => CRB, 1 => Socket CFG, 2 => TX Buffer, 3 => RX Buffer
     * @param sock_no       Socket number to access. CRB only available for sock_no = 0, otherwise 0 to 7
     * @param offset        Offset (in bytes) to read from
     * @param rx_buffer     Pointer to the buffer that should be written into
     * @param num           Number of bytes to read.
     */
    void read(uint8_t regtype, uint8_t sock_no, uint16_t offset, void * rx_buffer, size_t num);
    /**
     * @brief Write to a given register of the W5500
     * 
     * @param regtype       Register type. 0 => CRB, 1 => Socket CFG, 2 => TX Buffer, 3 => RX Buffer
     * @param sock_no       Socket number to access. CRB only available for sock_no = 0, otherwise 0 to 7
     * @param offset        Offset (in bytes) to write to.
     * @param tx_buffer     Pointer to a buffer whose data will be transferred to the W5500
     * @param num           Number of bytes to transfer out.
     */
    void write(uint8_t regtype, uint8_t sock_no, uint16_t offset, const void * tx_buffer, size_t num);

    //! Wrapper function to easily read a common-register-block register. @sa read
    void read_crb(REGS::crb_offsets reg, void * rx_buffer, size_t num);
    //! Wrapper function to easily write to a common-register-block register. @sa write
    void write_crb(REGS::crb_offsets reg, const void * tx_buffer, size_t num);

    /**
     * @brief Claim a socket.
     * @details This function allows a given BaseSocket to attempt to obtain
     *   a socket number for operation. As the W5500 has eight free sockets,
     *   each BaseSocket must be assigned one of these socket numbers.
     * 
     * @param sock_ptr Reference to the BaseSocket that is claiming the socket
     * @return int 0-7 for a successful claim, -1 if all sockets are currently in use.
     */
    int claim_socket(BaseSocket & sock_ptr);

    //! Release a given socket and make it available again.
    void release_socket(int i);

public:
    /**
     * @brief Construct a new W5500 core object 
     * @details This will construct a new W5500 object, to be used to 
     *   communicate with a W5500 PHY via SPI bus. The necessary definition
     *   for the SPI lines MUST be given to the SPI::Device, a Chip-Select line is
     *   necessary!
     * 
     *   After construction, call init(), then set up the chip address, before 
     *   calling loop_isr() to properly handle interrupt notifications.
     * 
     *   It is also recommended to set up a external interrupt line that
     *   triggers notify_isr() to minimize latency (else the ISR flags must be
     *   polled).
     * @param device SPI::Device, holding the configuration for the chipselect lines etc.
     */
    Core(SPI::Device device);
    
    /**
     * @brief Initialize the W5500
     * @details This function will attempt to initialize the W5500. A soft reset
     *   is performed, followed by setup of the PHY. The system then waits 
     *   for the PHY to connect. FreeRTOS Mutexes are also created here.
     * 
     *   After calling init(), the IPv4 address should be configured via 
     *   configure_address() or fetch_address(). Then, BaseSockets may be
     *   started.
     * 
     */
    void init();

    void enable_int();

    //! Call from an EXTI ISR on the ISR line of the W5500, to notify the processing thread 
    void notify_from_isr();
    //! MUST be called from a FreeRTOS thread dedicated to the W5500. Will loop indefinitely.
    void loop_isr() __attribute__((__noreturn__));

    /**
     * @brief Configure a static address for the W5500
     * @details This will set up a static IPv4, Gateway IP, Subnet mask 
     *   and allow the user to change the MAC address. 
     *   Use the W5500_ASSIGN_IP macro to populate the REGS::crb_address_config_t struct.
     * 
     * @param config Struct containing the address configuration to use
     */
    void configure_address(const REGS::crb_address_config_t & config);

    /**
     * @brief Returns true if the W5500 has been successfully initialized
     */
    bool is_initialied();
    /**
     * @brief Returns true if the W5500 has successfully established a phy link.
     *   May return to false if link is lost, for whatever reason
     */
    bool has_link();
};

}
 
