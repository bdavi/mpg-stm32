/*
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "w5500.h"

#define LOG_LEVEL LOG_DEBUG
#include <all/log/log.h>

namespace W5500 {

using namespace REGS;

static const uint16_t RECONNECT_DELAYS[] = { 0, 0, 500, 1000, 1000,
		3000, 3000, 3000,
		5000, 5000, 5000, 5000, 10000 };

BaseSocket::BaseSocket(Core & phy_core)
	: 	sock_no(-1), 
		current_sock_state(CLOSED), current_config(),
	  waiting_on_write(nullptr), waiting_on_read(nullptr),
	  write_pending_packet_data(0),
	  last_write_ptr(-1),
	  reconnect_tick(0), reconnect_count(0),
	  phy_core(phy_core),
	  event_cb_data(nullptr), on_event_cb(nullptr) {
}
BaseSocket::~BaseSocket() {
	close();
}

void BaseSocket::plan_reconnect() {
	// Fetch pre-set reconnect delay times from the definition
	auto wait_time = RECONNECT_DELAYS[std::min<int>(sizeof(RECONNECT_DELAYS)/2-1, reconnect_count)];

	if(wait_time == 0)
		reopen_socket();
	else
		reconnect_tick = xTaskGetTickCount() + wait_time;

	reconnect_count++;
}

void BaseSocket::reopen_socket() {
	if(sock_no == -1) {
		reconnect_tick = 0;
		return;
	}

	uint8_t cmd = SOCK_CR_OPEN;
	write_mr(SOCK_COMMAND, &cmd, 1);
	if(current_config.mode == TCP_SERVER) {
		cmd = SOCK_CR_LISTEN;
		write_mr(SOCK_COMMAND, &cmd, 1);
	}
	else if(current_config.mode == TCP_CLIENT) {
		cmd = SOCK_CR_CONNECT;
		write_mr(SOCK_COMMAND, &cmd, 1);
	}
}

void BaseSocket::check_isr() {
	sock_isr_t isr;
	read_mr(SOCK_ISR, &isr, 1);
	write_mr(SOCK_ISR, &isr, 1);

	if(isr.send_ok) {
		LOGV("W5500", "Send on socket %p completed!", this);
		if(waiting_on_write)
			xTaskNotify(waiting_on_write, 0, eNoAction);
		else if(on_event_cb)
			on_event_cb(event_cb_data, TX_COMPLETE);
	}
	if(isr.recv) {
		LOGV("W5500", "Packet receive indicated on socket %p", this);
		if(waiting_on_read)
			xTaskNotify(waiting_on_read, 0, eNoAction);
		else if(on_event_cb)
			on_event_cb(event_cb_data, RX_COMPLETE);
	}
	if(isr.con) {
		current_sock_state = SOCK_ESTABLISHED;
		reconnect_tick  = 0;
		reconnect_count = 0;
		write_pending_packet_data = 0;

		if(waiting_on_read)
			xTaskNotify(waiting_on_read, 0, eNoAction);
		if(waiting_on_write)
			xTaskNotify(waiting_on_write, 0, eNoAction);

		if(on_event_cb)
			on_event_cb(event_cb_data, CONNECTED);
	}
	if(isr.discon || isr.timeout) {
		current_sock_state = CLOSED;

		if(waiting_on_read)
			xTaskNotify(waiting_on_read, 0, eNoAction);
		if(waiting_on_write)
			xTaskNotify(waiting_on_write, 0, eNoAction);

		if(on_event_cb)
			on_event_cb(event_cb_data, DISCONNECTED);
	}

	if(current_config.mode == TCP_SERVER) {
		if(isr.timeout || isr.discon) {
			LOGI("W5500", "Client disconnected from port %d, re-opening.", current_config.source_port);

			reopen_socket();
		}

		if(isr.con) {
			auto & dest_ip = current_config.dest_ip;

			read_mr(SOCK_DEST_IP, &dest_ip, 4);

			LOGI("W5500", "Client %d.%d.%d.%d connected to port %d!",
					dest_ip[0], dest_ip[1], dest_ip[2], dest_ip[3],
					current_config.source_port);
		}
	}

	if(current_config.mode == TCP_CLIENT) {
		if(isr.con) {
			LOGD("W5500", "Connected to %d.%d.%d.%d:%d",
					W5500_EXP_IP(current_config.dest_ip), current_config.dest_port);
		}
		if(isr.timeout) {
			LOGE("W5500", "Timeout from server %d.%d.%d.%d:%d (try %d)",
					W5500_EXP_IP(current_config.dest_ip), current_config.dest_port, reconnect_count);

			plan_reconnect();
		}
		if(isr.discon) {
			LOGE("W5500", "Disconnect from server %d.%d.%d.%d:%d (try %d)",
					W5500_EXP_IP(current_config.dest_ip), current_config.dest_port, reconnect_count);

			plan_reconnect();
		}
	}
}

void BaseSocket::handler_tick() {
	if(sock_no == -1)
		return;

	if((reconnect_tick != 0) && (int32_t(xTaskGetTickCount() - reconnect_tick) > 0)) {
		LOGD("W5500", "Attempting reconnect!");
		reconnect_tick = xTaskGetTickCount() + 3000;
		reopen_socket();
	}

	if(on_event_cb)
		on_event_cb(event_cb_data, TICK);
}

void BaseSocket::phy_disconnected() {
	if(sock_no == -1)
		return;

	if(current_config.mode == TCP_CLIENT || current_config.mode == TCP_SERVER) {
		reopen_socket();
	}
}

void BaseSocket::write_mr(REGS::sock_offsets reg, const void * tx_buffer, size_t num) {
	if(sock_no < 0)
		return;

	phy_core.write(1, sock_no, reg, tx_buffer, num);
}
void BaseSocket::read_mr(REGS::sock_offsets reg, void * rx_buffer, size_t num) {
	if(sock_no < 0)
		return;

	phy_core.read(1, sock_no, reg, rx_buffer, num);
}

void BaseSocket::write_u16(sock_offsets reg, uint16_t port) {
	port = ((port & 0xFF) << 8) | ((port & 0xFF00) >> 8);
	write_mr(reg, &port, 2);
}
uint16_t BaseSocket::read_u16(sock_offsets reg) {
	uint16_t out = 0;
	read_mr(reg, &out, 2);

	return ((out & 0xFF) << 8) | ((out & 0xFF00) >> 8);
}

void BaseSocket::start_tcp_server() {
	LOGD("W5500", "Starting TCP server on socket no. %d, port %d!", sock_no, current_config.source_port);

	sock_mode_register_t mr = {};
	mr.mode = 1;
	mr.nodelay_multicast = 1;
	write_mr(SOCK_MODE, &mr, 1);

	write_u16(SOCK_SOURCE_PORT, current_config.source_port);

	uint8_t cmd;
	cmd = 1;
	write_mr(SOCK_KEEPALIVE, &cmd, 1);

	cmd = SOCK_CR_OPEN;
	write_mr(SOCK_COMMAND, &cmd, 1);


	sock_status_type status;
	for(int i=20; i!=0; i--) {
		read_mr(SOCK_STATUS, &status, 1);

		if((status == SOCK_INIT)) {
			break;
		}

		vTaskDelay(10);
	}
	if(status != SOCK_INIT) {
		LOGE("W5500", "Socket did not initialize!");
		phy_core.release_socket(sock_no);
		sock_no = -1;
		return;
	}

	cmd = SOCK_CR_LISTEN;
	write_mr(SOCK_COMMAND, &cmd, 1);

	for(int i=20; i!=0; i--) {
		sock_status_type status;
		read_mr(SOCK_STATUS, &status, 1);

		if((status == SOCK_LISTEN)) {
			LOGI("W5500", "TCP Server on port %d opened!", current_config.source_port);

			return;
		}

		vTaskDelay(10);
	}

	LOGE("W5500", "TCP Server on port %d failed to open!", current_config.source_port);

	phy_core.release_socket(sock_no);
	sock_no = -1;
}

void BaseSocket::start_tcp_client() {
	auto & t_ip = current_config.dest_ip;
	LOGD("W5500", "Starting TCP client connected to %d.%d.%d.%d:%d",
			t_ip[0], t_ip[1], t_ip[2], t_ip[3], current_config.dest_port);

	sock_mode_register_t mr = {};
	mr.mode = 1;
	write_mr(SOCK_MODE, &mr, 1);

	write_u16(SOCK_SOURCE_PORT, current_config.source_port);
	write_u16(SOCK_DEST_PORT, current_config.dest_port);

	write_mr(SOCK_DEST_IP, current_config.dest_ip, 4);

	uint8_t cmd;
	cmd = 1;
	write_mr(SOCK_KEEPALIVE, &cmd, 1);

	cmd = SOCK_CR_OPEN;
	write_mr(SOCK_COMMAND, &cmd, 1);
	vTaskDelay(1);

	sock_status_type status;
	read_mr(SOCK_STATUS, &status, 1);

	if(status != SOCK_INIT) {
		phy_core.release_socket(sock_no);
		sock_no = -1;
		LOGE("W5500", "Socket failed to init!");

		return;
	}

	cmd = SOCK_CR_CONNECT;
	write_mr(SOCK_COMMAND, &cmd, 1);

	reconnect_tick = xTaskGetTickCount() + 3000;
}

void BaseSocket::start_udp() {
	auto & t_ip = current_config.dest_ip;
	LOGD("W5500", "Opening UDP Port %d, sending to %d.%d.%d.%d:%d",
			current_config.source_port,
			t_ip[0], t_ip[1], t_ip[2], t_ip[3], current_config.dest_port);

	sock_mode_register_t mr = {};
	mr.mode = 2;
	write_mr(SOCK_MODE, &mr, 1);

	write_u16(SOCK_SOURCE_PORT, current_config.source_port);
	write_u16(SOCK_DEST_PORT, current_config.dest_port);

	write_mr(SOCK_DEST_IP, current_config.dest_ip, 4);

	uint8_t cmd = SOCK_CR_OPEN;
	write_mr(SOCK_COMMAND, &cmd, 1);
	vTaskDelay(1);

	sock_status_type status;
	read_mr(SOCK_STATUS, &status, 1);

	if(status != SOCK_UDP) {
		phy_core.release_socket(sock_no);
		sock_no = -1;
		LOGE("W5500", "Socket failed to init!");

		return;
	}
}

void BaseSocket::start(const socket_config_t & config) {
	if(!phy_core.chip_initialized) {
		LOGE("W5500", "Attempted opening a socket on an uninitialized chip. Aborting!");
		return;
	}

	current_config = config;

	sock_no = phy_core.claim_socket(*this);
	if(sock_no == -1) {
		LOGF("W5500", "ERROR, no free sockets left, could not claim for socket %p", this);
		return;
	}

	switch(config.mode) {
	case TCP_SERVER:
		start_tcp_server();
	break;

	case TCP_CLIENT:
		start_tcp_client();
	break;

	case UDP:
		start_udp();
	break;

	default:
		LOGF("W5500", "Unimplemented socket protocol chosen!");
		vTaskDelay(50);
		assert(false);
	break;
	}

	uint8_t socket_isr_mask = 0b00011111;
	write_mr(SOCK_ISR_MSK, &socket_isr_mask, 1);
}

void BaseSocket::wait_for_connect() {
	switch(current_config.mode) {
	default: return;
	case TCP_SERVER:
	case TCP_CLIENT:
		waiting_on_write = xTaskGetCurrentTaskHandle();

		while((sock_no != -1) && (current_sock_state != SOCK_ESTABLISHED)) {
			xTaskNotifyWait(0, 0, 0, 100/portTICK_PERIOD_MS);
		}

		waiting_on_write = nullptr;
	}
}

void BaseSocket::wait_for_receive() {
	if(sock_no == -1)
		return;

	waiting_on_read = xTaskGetCurrentTaskHandle();
	xTaskNotifyWait(0, 0, 0, portMAX_DELAY);
	waiting_on_read = nullptr;
}

bool BaseSocket::is_connected() {
	if(sock_no == -1)
		return false;
	if(!phy_core.phy_present)
		return false;

	switch(current_config.mode) {
		default:  return false;
		case UDP: return true;
		case TCP_CLIENT: 
		case TCP_SERVER:
			return current_sock_state == SOCK_ESTABLISHED;
	}
}

bool BaseSocket::is_open() {
	return sock_no != -1;
}

void BaseSocket::close() {
	if(sock_no >= 0) {
		phy_core.release_socket(sock_no);
		sock_no = -1;

		if(waiting_on_write)
			xTaskNotify(waiting_on_write, 0, eNoAction);
		if(waiting_on_read)
			xTaskNotify(waiting_on_read, 0, eNoAction);

		// Delays to allow the RW tasks to run regardless of this tasks's priority.
		vTaskDelay(5);

		uint8_t socket_isr_mask = 0;
		write_mr(SOCK_ISR_MSK, &socket_isr_mask, 1);

		// Send a closing command to the W5500's socket
		uint8_t close_cmd = SOCK_CR_CLOSE;
		write_mr(SOCK_COMMAND, &close_cmd, 1);
	}
}

size_t BaseSocket::get_free_tx() {
	return read_u16(SOCK_TX_FREE_SIZE);
}

bool BaseSocket::add_tx(const void * data, size_t num) {
	// Return false if the packet data size would be above MTU
	if((num + write_pending_packet_data) > 1500)
		return false;

	write_pending_packet_data += num;

	// First wait for the TX buffer to free up enough to 
	// serve us. Note that send delays will
	// likely be brief as the W5500's ethernet speed can
	// reach 100MBit/s, SPI only about 40MBit/s
	int retry_count = 0;
	while(true) {
		if((!is_connected()) || retry_count++ >= 10)
			return false;
		
		if(get_free_tx() >= num)
			break;

		waiting_on_write = xTaskGetCurrentTaskHandle();
		// Intentional short blocks as the buffer shouldn't stay full for long.
		// This really just helps prevent accidental deadlocks
		xTaskNotifyWait(0, 0, nullptr, 10/portTICK_PERIOD_MS);
		waiting_on_write = nullptr;
	}


	last_write_ptr = read_u16(SOCK_TX_WRITE_PTR);
	uint16_t write_head = last_write_ptr;
	phy_core.write(2, sock_no, write_head, data, num);

	last_write_ptr = 0xFFFF & (write_head + num);
	write_u16(SOCK_TX_WRITE_PTR, last_write_ptr);

	return true;
}

bool BaseSocket::send() {
	if(!is_connected())
		return false;

	uint8_t cmd = SOCK_CR_SEND;
	write_mr(SOCK_COMMAND, &cmd, 1);

	write_pending_packet_data = 0;

	return true;
}

bool BaseSocket::dump_tx(const void * data, size_t num) {
	// We're gonna do this real simple, send 1kB chunks using add_tx and send(), 
	// nothing more. Hope you don't need any framing :P

	auto r_data_ptr = reinterpret_cast<const uint8_t*>(data);

	while(num) {
		if(!is_connected())
			return false;

		auto chunk_len = std::min<size_t>(1024, num);
		if(!add_tx(r_data_ptr, chunk_len))
			return false;

		r_data_ptr += chunk_len;
		num  -= chunk_len;

		send();
	}

	return true;
}

size_t BaseSocket::get_rx_available() {
	if(!is_connected())
		return 0;

	return read_u16(SOCK_RX_SIZE);
}

bool BaseSocket::peek_rx(void * out_buffer, size_t num, size_t offset) {
	if(!is_connected())
		return false;

	if((num + offset) > get_rx_available())
		return false;

	uint16_t read_head = read_u16(SOCK_RX_READ_PTR) + offset;
	phy_core.read(3, sock_no, read_head, out_buffer, num);

	return true;
}

bool BaseSocket::free_rx(size_t num) {
	if(!is_connected())
		return false;

	write_u16(SOCK_RX_READ_PTR, 0xFFFF & (read_u16(SOCK_RX_READ_PTR) + std::min<size_t>(get_rx_available(), num)));
	uint8_t cmd = SOCK_CR_RECV;
	write_mr(SOCK_COMMAND, &cmd, 1);

	return true;
}

}
