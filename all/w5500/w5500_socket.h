/**
 * @file all/w5500/w5500_socket.h
 * @author David Bailey (david.bailey@ipp.mpg.de)
 * @brief 
 * @version 0.1
 * @date 2022-06-02
 * 
 * @copyright Copyright (c) 2022 David Bailey
 * SPDX-FileCopyrightText: 2022 David Bailey <davidbailey.2889@gmail.com>
 * SPDX-License-Identifier: MIT
 * 
 */

#pragma once

#include "defs.h"
#include <array>

namespace W5500 {

class Core;

/**
 * @brief Socket base class to establish TCP or UDP connection and perform raw socket I/O
 * @details This class implements all necessary functions to configure, connect and use 
 *   a W5500 socket. 
 *   It allows the user to very quickly and easily establish a TCP server, UDP port 
 *   or TCP connection. For TCP, auto-reopening of the server port or auto-reconnect 
 *   to a TCP server with backoff is also implemented, further reducing user-side 
 *   effort.
 * 
 *   Blocking R/W actions as well as receive-callbacks make it very easy to write simple 
 *   or complex communication protocols. 
 *   Please note, however, that no write mutex is supplied! Reading and Writing can happen
 *   simultaneously and require no mutex, and it is intended to use a socket from 
 *   one handler thread. If mutual write access is required, and external mutex may 
 *   be supplied.
 * 
 *   After initializing the socket with start(), the user may immediately begin 
 *   sending or receiving data using fill_buffer() & send(), or dump_buffer().
 *   A callback function can be supplied which is called after connecting, disconnecting
 *   and receiving data.
 * 
 *   The socket automatically closes in its destructor, allowing easy temporary sockets 
 *   to be created for one-off R/W operations within threads.
 *   @code
void test_tcp_task() {
	BaseSocket test_socket(w5500_core);

	socket_mode_t socket_config = {};
	socket_config.mode = TCP_CLIENT;
	W5500_ASSIGN_IP(socket_config.dest_ip, 192, 168, 178, 123);
	socket_config.dest_port = 80;
	socket_config.source_port = 9999;

	test_socket.start(socket_config);

	// For TCP, the socket buffer can either be filled in 
	// pieces and then sent out as one packet (maximum size 1.5kB), or, if 
	// sending as one single packet is not required,
	// dump_buffer can be used to send large buffers (unlimited) in multiple
	// packets. The user is then responsible for adding delimiters to 
	// these packets!
	test_socket.fill_buffer("Hello world!", 12);
	test_socket.send();

	vTaskDelay(10);
	test_socket.dump_buffer("Hello World!", 12);


	// To receive a buffer, recv provides a flexible way of
	// fetching data, either to specific length, delimiter, and 
	// with timeout.
	char in_buffer[256] = {};
	size_t len = 256;

	test_socket.recv(in_buffer, 0, &len, portMAX_DELAY);
}

 *   @endcode
 * 
 *   Please note that no protocol handling is implemented in this class. Overloaded classes 
 *   may be used to handle specific protocols.
 * 
 * @todo The callback system should be rewritten to offload
 *   user-code onto a separate FreeRTOS Thread. As-is, the 
 *   user-code callbacks could theoretically block while waiting to
 *   write, after the socket disconnected, or otherwise delays 
 *   handling of critical interrupts
 */
class BaseSocket {
public:
	//! Socket mode enum, defines in what mode the socket will behave.
	enum socket_mode_t {
		UNUSED,			//!< Socket is currently inactive and requires configuration.
		TCP_CLIENT,		//!< Socket is configured as TCP client.
		TCP_SERVER,		//!< Socket is configured as TCP server
		UDP				//!< Socket is configured as UDP socket for raw rw
	};

	//! Socket event enum, used to tell the on_event_cb function what happened.
	enum socket_event_t {
		CONNECTED,		//!< Emitted in TCP mode for Server and Client alike
		RX_COMPLETE,	//!< Emitted in all modes when new data is present in RX buffer
		TX_COMPLETE,	//!< Emitted in TCP mode when data was sent. @todo confirm
		TICK,			//!< Emitted once a second for user-defined regular maintenance work
		DISCONNECTED	//!< Emitted in TCP mode on a client disconnect
	};

	/**
	 * @brief User Socket configuration register.
	 * @details This register is meant to let the user easily pass configuration about 
	 *   how this socket should behave. It is used for the initial BaseSocket.start call, 
	 *   to set up necessary parameters.
	 */
	struct socket_config_t {
		socket_mode_t mode;		//!< Mode for the socket to be in.
		uint16_t source_port;	//!< Source port. Unused for TCP Client, local socket for TCP Server and UDP mode.
		uint16_t dest_port;		//!< Destination port. Unused for TCP Server, required for TCP client and UDP mode.
		uint8_t  dest_ip[4];	//!< Destination IP to connect to. Unused for TCP server, required for TCP client and UDP.
	};

private:
	// Socket number of the W5500 used here (0-7) or -1 if it is uninitialized
	int sock_no;
	// Last socket status as reported by the SOCK_STATUS register
	REGS::sock_status_type current_sock_state;

	// Current configuration. Used when closing and re-opening the socket, when 
	// a reconnect is necessary, etc.
	socket_config_t current_config;

	//! Pointer to the thread that is currently waiting on the write buffer to 
	//! free up.
	TaskHandle_t waiting_on_write;
	//! Pointer to the thread that is waiting on a read to be received. 
	//! If non-null, will circumvent the read callback. Please note that 
	//! the read callback IS preferred over blocking reads, if possible.
	TaskHandle_t waiting_on_read;

	// Amount of data not yet sent by a send() command
	size_t write_pending_packet_data;

	// Last socket write pointer. This is useful to avoid
	// reading the write pointer on each access, saving a
	// SPI transaction
	int32_t last_write_ptr;

	//! FreeRTOS Tick after which to force a reconnect. Used for internal auto-backoff
	//! reconnecting in TCP Client mode, unused for TCP Server and UDP socket.
	TickType_t reconnect_tick;
	//! Counts up for each unsuccessful reconnect attempt. Used for the TCP Client auto-backoff.
	//! Please note that there is no limit to TCP reconnection. The system will indefinitely 
	//! attempt to reconnect.
	int reconnect_count;

	//! Wrapper function to quickly write to the socket Mode Register for config
	void write_mr(REGS::sock_offsets reg, const void * tx_buffer, size_t num);
	//! Wrapper function to quickly read from the socket mode register
	void read_mr(REGS::sock_offsets reg, void * rx_buffer, size_t num);

	//! Convenience function to write a u16 value to the Mode Register (endianness swap needed).
	void write_u16(REGS::sock_offsets, uint16_t data);
	//! Convenience function to read a u16 value from the Mode Register (with endianness swap)
	uint16_t read_u16(REGS::sock_offsets);

	//! Called after a failed connection attempt, increments reconnect_count and sets reconnect_tick
	void plan_reconnect();
	//! Called to close, then immediately re-open a given socket, and forces or starts a reconnect.
	void reopen_socket();

	//! Starts the socket in TCP Server mode, with port taken from current_config. Called by start()
	void start_tcp_server();
	//! Starts the socket in TCP client mode, waits for connection, then returns.
	void start_tcp_client();
	//! Starts an open UDP socket
	void start_udp();

protected:
//! The core must be allowed access to these functions to properly call 
//! the ISR check.
friend Core;
	/**
	 * @brief Interrupt check function, called by the Core.
	 * @details This function is called when the Core reads that this socket's ISR
	 *   flag was set. It will read the socket interrupt register and take 
	 *   necessary action as follows:
	 *  - Upon a connect or disconnect event it will notify any waiting task (waiting_on_write, waiting_on_read),
	 *    and will then either initialize a reconnect (via plan_reconnect()), or call the
	 *    callback with "CONNECTED" flag.
	 *  - Upon a SEND event it will notify the waiting_on_write task
	 *  - Upon a RECV event it will first try to notify the waiting_on_read task. 
	 *    If no task is waiting on read, it will then call a given callback, and set the "Data present"
	 *    flag.
	 * 
	 */
	void check_isr();
	/**
	 * @brief Optional regular connection maintenance
	 * @details This function will be called once a second by the Core. This 
	 *   call is then propagated to the callback function, and allows the user 
	 *   code to easily perform timed maintenance of the socket connection.
	 */
	void handler_tick();

	//! Called by the handler when a physical disconnect occurred.
	//! This could theoretically leave a connection awkwardly hanging,
	//! so we disconnect in TCP mode
	void phy_disconnected();

public:
	//! Reference to the W5500::Core that is used for this connection
	Core & phy_core;

	//! Optional data passed into the on_event_cb
	void * event_cb_data;
	/**
	 * @brief Event callback for this socket
	 * @details This callback MAY be supplied by the user to allow for 
	 *   asynchronous handling of socket events. It is called for the events
	 *   listed in socket_event_t.
	 *   This callback is called from within the W5500's handler thread, and
	 *   may thusly block shortly. Care should still be taken to finish quickly!
	 *   Due to the W5500's fast ethernet PHY it is feasible to wait on TX for
	 *   small amounts of data. As such, the full RX/TX set of functions may 
	 *   be used within the callback events to send/receive data and process 
	 *   it.
	 * 
	 *   Also note that 
	 */
	void (*on_event_cb)(void * arg, socket_event_t evt);

	/**
	 * @brief Construct a new socket.
	 * @details This will construct a W5500 socket, but will not initialize it,
	 *   nor allocate connection in the chip itself. This means it does not 
	 *   use any socket number yet.
	 * 
	 *   After initializing, and once the Core has been initialized (refer to 
	 *   Core.init()), this socket can be started by calling start() with 
	 *   appropriate configuration. If necessary, start() will block 
	 *   until a connection has been established. 
	 *   The socket can then immediately be used for R/W operations.
	 * 
	 *   close() can be called to close the socket and disconnect, 
	 *   freeing it from the W5500's list of used sockets.
	 *   The deconstructor of the BaseSocket also handles this process,
	 *   allowing easy temporary sockets.
	 * 
	 * @param phy_core Reference to the Core to use for connection.
	 */
	BaseSocket(Core &phy_core);

	/**
	 * @brief Destroy the socket
	 * @details This will safely destroy the socket, notifying 
	 *   any potentially waiting threads, then closing the 
	 *   socket and de-registering itself from the W5500.
	 *   It is thusly safe under all circumstances, provided that
	 *   any waiting threads check if the port pointer is still valid!
	 * 
	 */
	~BaseSocket();

	/**
	 * @brief (Re)start the Port with previously given config.
	 * @details This will simply call start(const socket_config_t &) with the previously
	 *   supplied socket configuration. It is thusly a convenient shorthand to quickly
	 *   re-open the socket when needed.
	 * 
	 *   Note that auto-reconnect handling on unexpected disconnects is handled 
	 *   by the socket itself! This function is meant to be used after an 
	 *   intentional disconnect.
	 * 
	 */
	void start();
	/**
	 * @brief Open the socket with given configuration.
	 * @details This will open the socket based on the given configuration.
	 *   See socket_config_t for its options. Three possible modes 
	 *   have been implemented: 
	 *   - TCP Server
	 *   - TCP Client
	 *   - UDP (bidirectional)
	 * 
	 *   For TCP, wait_for_connect() will yield until connection has been established (server & client).
	 *   For UDP, no wait is necessary. 
	 *   The socket can then be used to send/receive data. Please note that data reads/writes to a 
	 *   disconnected socket will return immediately with "false" return code!
	 * 
	 *   It is advised to use a state machine and the on_event_cb to handle 
	 *   receiving of data, to avoid spawning extra threads. The on_event_cb 
	 *   callback is executed in the general W5500 handler thread, and may 
	 *   block briefly, but it is recommended to return ASAP.
	 * 
	 * @param config socket_config_t configuration to use.
	 */
	void start(const socket_config_t & config);

	//! @brief Wait for connection establishing in TCP mode after connection loss.
	//! @warning This function may return early when close() is called!
	void wait_for_connect();
	//! @brief Wait for a new data packet to be received.
	//! @warning This function may return early when close() is called!
	void wait_for_receive();

	/**
	 * @brief Returns true if a valid connection is established.
	 * @details This function may be used to avoid dropping packets,
	 *   as the TX functions will silently drop packets when no 
	 *   valid connection has been established. In this case,
	 *   wait_for_connect() can be used to wait for a re-established
	 *   connection.
	 * 
	 *   Will always return true in UDP mode while socket is open.
	 * @return true 
	 * @return false 
	 */
	bool is_connected();

	/**
	 * @brief Return true if the socket is open
	 * @details This function will always return true after a call to
	 *   start() with valid configuration (unless no free ports were found
	 *   in the W5500).
	 *   The difference between this and is_connected() is that this function
	 *   is purely related to the socket configuration, whereas is_connected() 
	 *   requires an established connection.
	 * 
	 * @return true 
	 * @return false 
	 */
	bool is_open();

	/**
	 * @brief Close the socket fully and disable auto-reconnect.
	 * @details This will mark the socket as closed, and will 
	 *   disconnect and de-register it from the W5500 fully.
	 *   It will make all rx and tx functions return with "false",
	 *   wait_for_connect and wait_for_receive will return immediately.
	 * 
	 *   The port can be re-opened with start()
	 */
	void close();

	//! @brief Return the number of free bytes left in the TX buffer.
	size_t get_free_tx();

	/**
	 * @brief Append some data to the TX buffer for sending.
	 * @details This will queue up data to the internal buffer
	 *   without sending it. send() triggers transmission of 
	 *   a packet. If the buffer is too full, it will wait 
	 *   on it to free up. 
	 * @note It is recommended to stay below 
	 *   the connection MTU (~1.4kB) for a single packet, 
	 *   else the data might become fragmented!
	 * @warning If more than 2kB is supplied via add_tx() without a call to send(),
	 * 	 this function may hang indefinitely as it waits for the W5500 buffer 
	 *   to free up, which never happens as nothing is being sent!
	 *   Dumping larger potions of data that may be fragmented is possible 
	 *   using dump_tx(), or by sending data in packets of ~1kB and calling
	 *   send() after each.
	 *  
	 *   It might be possible to utilize the W5500's buffer redistribution to
	 *   give a socket more than 2kB, however this is not currently implemented.
	 * 
	 * @sa send()
	 * @sa dump_tx()
	 * 
	 * @param data Pointer to the buffer to be sent out
	 * @param num  Number of bytes to add to the TX buffer
	 * @return true If data was successfully written to buffer
	 * @return false If client disconnected while attempting to add data to buffer
	 *   or no client was connected.
	 */
	bool add_tx(const void * data, size_t num);

	/**
	 * @brief Send queued-up data from buffer.
	 * @details This function will trigger the SEND function of the buffer.
	 *   This will send all data previously added via add_tx as a single buffer,
	 *   if it stayed below the MTU of about ~1.5kB. A maximum packet size
	 *   of 1kB is recommended.
	 * 
	 * @return true 	If data could be sent successfully
	 * @return false 	If the client disconnected, no client is connected or socket is closed.
	 */
	bool send();

	/**
	 * @brief Dump a large buffer to the socket.
	 * @details This function can accept any length of buffer, and will
	 *   automatically dump it into the socket, calling send() when necessary. 
	 *   Note that this will fragment any data above 1kB for sure, 
	 *   and it MAY fragment any smaller data if it does not fit into the 
	 *   current buffer. If manual handling of fragmentation is required,
	 *   use add_tx() and send(). This function is mostly for convenience 
	 *   of sending large buffers of data with known formatting and clear 
	 *   end or length indicators that can withstand fragmentation.
	 * 
	 * @param data 
	 * @param num 
	 * @return true 
	 * @return false 
	 */
	bool dump_tx(const void * data, size_t num);

	//! @brief Returns number of bytes waiting in the RX buffer.
	//! @details Always returns 0 when disconnected or closed.
	//! @note It is not possible to determine the length of a single packet.
	//!  If two packets arrive in quick succession, before the RX callback can 
	//!  handle it, it will appear as one large packet!
	//! @sa peek_rx()
	size_t get_rx_available();

	/**
	 * @brief Peek into the RX buffer.
	 * @details This function will copy out a number of bytes from 
	 *   the RX buffer, with optional offset from the start. If will
	 *   never copy out more bytes than are available in the RX buffer,
	 *   check with get_rx_available() first.
	 * 
	 * @note The read bytes are not freed from the buffer. Use 
	 *   drop_rx() afterwards to release them. Not doing this 
	 *   results in the W5500 socket RX dropping data!
	 * 
	 * @param out_buffer Buffer to copy into. Must be at least num bytes large!
	 * @param num 		 Number of bytes to copy out.
	 * @param offset 	 Optional offset to use when copying. Allows e.g. inspection
	 *   of control bytes for the used protocol.
	 * @return true 	Returns true if enough bytes were available to read.
	 * @return false 	Returns false if the user requested too many bytes, 
	 * 	or bytes from a closed socket.
	 */
	bool peek_rx(void * out_buffer, size_t num, size_t offset = 0);

	/**
	 * @brief Drop RX data from buffer.
	 * @details This function will free num bytes from the RX buffer.
	 *   It MUST be called after peek_rx to discard data that is no longer
	 *   needed. 
	 *   It is safe to call drop_rx with a size larger than get_rx_available(),
	 *   in which case the entire buffer will be dropped. 
	 * 
	 * @param num 		Number of bytes to drop from the buffer
	 * @return true 	True if the socket is connected
	 * @return false 	False if there is no established connection
	 */
	bool free_rx(size_t num);
};

}
