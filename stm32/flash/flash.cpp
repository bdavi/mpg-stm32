/*
 * flash.c
 *
 *  Created on: Sep 16, 2022
 *      Author: xaseiresh
 */

#include "flash.h"

#include <all/log/log.h>
#include <algorithm>

// The main.h of a STM32CubeIDE project always includes the HAL definition files
#include "main.h"

#define FLASH_BASE_ADDR 0x40023C00

#define FLASH_OPT (*reinterpret_cast<uint32_t*>(FLASH_BASE_ADDR + 0x14))
#define FLASH_OPT1 (*reinterpret_cast<uint32_t*>(FLASH_BASE_ADDR + 0x18))

#define FLASH_OPTKEYR (*reinterpret_cast<uint32_t*>(FLASH_BASE_ADDR + 0x8))

#define FLASH_OPT_UNLOCK_1 0x08192A3B
#define FLASH_OPT_UNLOCK_2 0x4C5D6E7F

namespace Flash {

struct flash_sector_def_t {
	int sector_start_no;
	uint32_t group_start;
	uint32_t group_end;
	uint32_t sector_size;
};

const flash_sector_def_t dual_bank_sector_definitions[] = {
		{ 0,  0x08000000, 0x0800FFFF, 0x4000 },
		{ 4,  0x08010000, 0x0801FFFF, 0x10000 },
		{ 5,  0x08020000, 0x080FFFFF, 0x20000 },
		{ 12, 0x08100000, 0x0810FFFF, 0x4000 },
		{ 16, 0x08110000, 0x0811FFFF, 0x10000 },
		{ 17, 0x08120000, 0x081FFFFF, 0x20000}
};

void FlashWriter::write_optregs(uint32_t opt, uint32_t opt1) {
	// Unlock the register first

	FLASH_OPTKEYR = FLASH_OPT_UNLOCK_1;
	FLASH_OPTKEYR = FLASH_OPT_UNLOCK_2;

	FLASH_OPT1 = opt1;
	FLASH_OPT  = opt & ~(0b11);

	// START the flash write operation
	FLASH_OPT  |= 0b10;

	// Re-Lock the Flash OPT registers
	FLASH_OPT |= 1;
}

int FlashWriter::banking_enabled() {
	// TODO FIX THIS!! This is only valid for 2MB devices that do not need a
	// separate banking-enable bit.
	return 1;

	// return (*reinterpret_cast<const uint32_t*>(OPTCR_BYTE1_ADDRESS) & (1<<30 || 1<<4)) != 0;
}
int FlashWriter::selected_bank() {
	return (FLASH_OPT & (1<<4)) >> 4;
}

void FlashWriter::select_bank(int no) {
	if(no < 0)
		no = 1 - selected_bank();

	uint32_t next_flash_opt = (FLASH_OPT & ~(1<<4)) | ((no&1) << 4);
	uint32_t next_flash_opt1 = FLASH_OPT1;

	LOGD("FLASH", "Would have programmed registers as: %x %x", next_flash_opt, next_flash_opt1);

	write_optregs(next_flash_opt, next_flash_opt1);

	NVIC_SystemReset();
}

// TODO This has to be adjusted for every STM32!!
int FlashWriter::get_sector_number(uint32_t location) {
	for(uint32_t i=0; i<sizeof(dual_bank_sector_definitions)/sizeof(flash_sector_def_t); i++) {
		auto c_sec = dual_bank_sector_definitions[i];
		if((location >= c_sec.group_start) && (location <= c_sec.group_end))
			return (location - c_sec.group_start) / c_sec.sector_size + c_sec.sector_start_no;
	}

	return -1;
}

FlashWriter::FlashWriter() :
		start_location(0),
		continue_location(0),
		end_location(0) {

}

void FlashWriter::unlock() {
	HAL_FLASH_Unlock();
}
void FlashWriter::lock() {
	HAL_FLASH_Lock();
}

bool FlashWriter::begin(uint32_t start, uint32_t end) {
	int sec_start = get_sector_number(start);
	int sec_end   = get_sector_number(end);

	if(sec_start == -1 || sec_end == -1)
		return false;

	LOGI("FLASH", "Erasing locations %x to %x!", start, end);
	LOGI("FLASH", "This will erase sectors %d through %d!!", sec_start, sec_end);

	start_location = start;
	end_location = end;

	continue_location = start;

	FLASH_EraseInitTypeDef flash_erase_cmd = {};
	flash_erase_cmd.TypeErase = FLASH_TYPEERASE_SECTORS;
	flash_erase_cmd.Sector = sec_start;
	flash_erase_cmd.NbSectors = sec_end - sec_start + 1;
	flash_erase_cmd.VoltageRange = FLASH_VOLTAGE_RANGE_3;

	uint32_t erase_error = 0;

	HAL_FLASHEx_Erase(&flash_erase_cmd, &erase_error);

	return true;
}

bool FlashWriter::append(uint32_t location, const void * data_raw, size_t num_bytes) {
	const uint8_t * data = reinterpret_cast<const uint8_t * >(data_raw);

	if(location != continue_location) {
		LOGE("FLASH", "Writing non-continuously is not allowed (tried writing to %x, should have written to %x)"
				, location, continue_location);
		return false;
	}

	if(location + num_bytes > end_location) {
		LOGE("FLASH", "Attempted writing larger than initially planned! (Tried writing to %x, end is %x)"
				, location+num_bytes, end_location);

		return false;
	}

	continue_location = location+num_bytes;

	// We have to start with a few byte-writes first to align to DWORD
	size_t num_alignment = std::min<size_t>(location & 7, num_bytes);
	while(num_alignment > 0) {
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, location, *data);
		location += 1;
		data += 1;
		num_bytes -= 1;
		num_alignment--;
	}

	size_t num_words = num_bytes / 4;
	while(num_words > 0) {
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, location, *reinterpret_cast<const uint32_t * >(data));
		location += 4;
		data += 4;
		num_bytes -= 4;
		num_words--;
	}

	while(num_bytes > 0) {
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, location, *data);
		location += 1;
		data += 1;

		num_bytes--;
	}

	return true;
}

bool FlashWriter::check_crc(uint32_t test_crc) {
	CRC->CR |= 1;

	for(uint32_t location = start_location; location < end_location; location += 4) {
		CRC->DR = *reinterpret_cast<const uint32_t *>(location);
	}

	if(CRC->DR == test_crc) {
		LOGI("FLASH", "Flash CRC confirmed! (Is: %x)", CRC->DR);
		return true;
	}
	else {
		LOGE("FLASH", "Flash CRC missmatch! Expected: %x, got %x!!", test_crc, CRC->DR);
		return false;
	}
}

}
